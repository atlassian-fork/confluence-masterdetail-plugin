package it.webdriver.com.atlassian.confluence.extra.masterdetail.rest;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

// This is a copy of the same class in the plugin module. We don't want to depend on the plugin from the
// integration tests, but we also don't want to extract a common API module with Model
// classes just now.
public class DetailsSummaryLines {
    @JsonProperty
    private final int currentPage;

    @JsonProperty
    private final int totalPages;

    @JsonProperty
    private final List<String> renderedHeadings;

    @JsonProperty
    private final List<DetailLine> detailLines;

    @JsonProperty
    private final boolean asyncRenderSafe;

    @JsonCreator
    private DetailsSummaryLines() {
        this(0, 0, null, null, true);
    }

    public DetailsSummaryLines(int currentPage, int totalPages, List<String> renderedHeadings, List<DetailLine> detailLines, boolean asyncRenderSafe) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.renderedHeadings = renderedHeadings;
        this.detailLines = detailLines;
        this.asyncRenderSafe = asyncRenderSafe;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<String> getRenderedHeadings() {
        return renderedHeadings;
    }

    public List<DetailLine> getDetailLines() {
        return detailLines;
    }
}
