package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.util.test.annotations.ExportedTestClass;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.google.inject.Inject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static org.hamcrest.Matchers.hasItem;

@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
@ExportedTestClass
public class MasterDetailsExportedTest extends AbstractPagePropertiesReportTest {

    @Inject
    public Timeouts timeouts;

    @Test
    public void testPagePropertiesReport() {
        final PagePropertiesReportMacro report = gotoReportPage();
        // Summary table == report table
        report.waitUntilSummaryTableLoaded();

        waitUntilEquals("Expected a single row in the page properties report table",
                1, report.getNumberOfRows());

        waitUntil("Expected properties page title in the report table",
                Queries.forSupplier(timeouts, report::getTitles),
                hasItem(propertiesPage.getTitle())
        );
    }
}
