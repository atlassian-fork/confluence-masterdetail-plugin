package it.webdriver.com.atlassian.confluence.extra.masterdetail.rest;

import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.test.api.model.person.UserWithDetails;
import com.atlassian.confluence.test.rest.api.ConfluenceRestSession;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import it.webdriver.com.atlassian.confluence.extra.masterdetail.AbstractPagePropertiesReportTest;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Collections;

import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * REST-tests the Page Properties Report macro.
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PagePropertiesReportRestTest extends AbstractPagePropertiesReportTest {

    private static final String LABEL_CQL = "label='" + label.getLabel() + "'";

    @Fixture
    public static PageFixture page = pageFixture()
            .title("page that anon can view")
            .space(space)
            .content(makeDefaultDetailsMarkup())
            .author(user)
            .build();

    @Fixture
    public static PageFixture childPage = pageFixture()
            .title("child page that anon can view")
            .space(space)
            .content(makeDefaultDetailsMarkup())
            .parent(page)
            .author(user)
            .build();

    @BeforeClass
    public static void addLabels() {
        rest.contentLabelService().addLabels(page.get().getId(), labelList);
        rest.contentLabelService().addLabels(childPage.get().getId(), labelList);
        rpc.getSystemComponent().flushIndexQueue();
    }

    @Test
    public void testReportPermissions() {
        final Label label = Label.builder("testReportPermissions").build();
        rest.contentLabelService().addLabels(propertiesPage.getId(), Collections.singletonList(label));
        rest.contentLabelService().addLabels(page.get().getId(), Collections.singletonList(label));
        rpc.getSystemComponent().flushIndexQueue();

        final String cql = "label='" + label.getLabel() + "'";

        // Try as unrestricted user, should get 2 pages.
        DetailsSummaryLines lines = getDetailsSummaryLines(user.get(), cql, ContentId.UNSET);
        assertThat(lines.getDetailLines().size(), is(2));

        // Try as restricted user, should get 1 page.
        lines = getDetailsSummaryLines(userWithPageRestrictions.get(), cql, ContentId.UNSET);
        assertThat(lines.getDetailLines().size(), is(1));

        // Try as restricted user again without space permission, should get 0 pages.
        rest.permissions().removeSpacePermissions(space.get(), userWithPageRestrictions.get(), SpacePermission.VIEW);
        lines = getDetailsSummaryLines(userWithPageRestrictions.get(), cql, ContentId.UNSET);
        assertThat(lines.getDetailLines().size(), is(0));

        // Clean up
        rest.permissions().addSpacePermissions(space.get(), userWithPageRestrictions.get(), SpacePermission.VIEW);
    }

    @Test
    public void testCurrentContentInCql() {
        // The child page should be present in the rendered macro lines that are served via REST.
        final String cql = LABEL_CQL + " and parent = currentContent()";
        DetailsSummaryLines lines = getDetailsSummaryLines(user.get(), cql, page.get().getId());
        assertThat(lines.getDetailLines().size(), is(1));
        assertThat(lines.getDetailLines().get(0).getTitle(), is(childPage.get().getTitle()));
    }

    @Test
    public void testNonAsyncMacroWorksWithPagination() {
        final Label label = Label.builder("testNonAsyncMacroWorksWithPagination")
                .id("testNonAsyncMacroWorksWithPagination")
                .build();
        final String cql = "label='" + label.getLabel() + "'";
        createPages(10, label);
        rpc.getSystemComponent().flushIndexQueue();

        // Make a REST request with page size 5
        DetailsSummaryLines lines = getDetailsSummaryLines(user.get(), cql, ContentId.UNSET, 5);
        assertThat(lines.getDetailLines().size(), is(5));
    }

    @Test
    public void testRenderWithConcurrency() {
        //trigger concurrency render. see DetailsSummaryBuilder#ROWS_PER_PARALLEL_RENDER
        final Label label = Label.builder("testRenderWithConcurrency")
                .id("testRenderWithConcurrency")
                .build();
        final String cql = "label='" + label.getLabel() + "'";
        createPages(101, label);
        rpc.getSystemComponent().flushIndexQueue();

        // Make a REST request with page size 20
        DetailsSummaryLines lines = getDetailsSummaryLines(user.get(), cql, ContentId.UNSET, 20);
        assertThat(lines.getDetailLines().size(), is(20));
        assertThat(lines.getCurrentPage(), is(0));
        assertThat(lines.getTotalPages(), is(6));
    }

    private DetailsSummaryLines getDetailsSummaryLines(final UserWithDetails user,
                                                       final String cql,
                                                       final ContentId currentContentId) {
        return getDetailsSummaryLines(user, cql, currentContentId, 30);
    }


    private DetailsSummaryLines getDetailsSummaryLines(final UserWithDetails user,
                                                       final String cql,
                                                       final ContentId currentContentId,
                                                       final int pageSize) {
        final ConfluenceRestSession restSession = user == null ?
                restClient.getAnonymousSession() : restClient.createSession(user);
        return restSession.resource()
                .path("/rest/masterdetail/1.0/detailssummary/lines")
                .queryParam("cql", cql)
                .queryParam("pageSize", String.valueOf(pageSize))
                .queryParam("contentId", currentContentId.serialise())
                .queryParam("spaceKey", space.get().getKey())
                .get(DetailsSummaryLines.class);
    }
}
