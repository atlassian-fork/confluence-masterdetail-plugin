package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.BlueprintBlankExperience;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
public class BlueprintPagePropertiesReportTest extends AbstractPagePropertiesReportTest {

    private final static String blankTitle = "blank title";
    private final static String blankDescription = "this is description of blank page";
    private final static String blueprintModuleCompleteKey = "com.atlassian.confluence.plugins." +
            "confluence-software-blueprints:requirements-blueprint";
    private final static String createButtonLabel = "create button label";

    @Test
    public void blueprintBlankExperience() {
        updateReportPage(makeBlueprintReport(Label.builder("label-that-isnt-on-anything" + System.currentTimeMillis()).build()));
        gotoReportPage();
        final BlueprintBlankExperience blueprintBlankExperience =
                product.getPageBinder().bind(BlueprintBlankExperience.class);

        waitUntil(blueprintBlankExperience.getText(), containsString(blankDescription));
        waitUntilTrue(blueprintBlankExperience.findLinkWithText(createButtonLabel).isVisible());
    }

    @Test
    public void blueprintBlankExperienceNotShownWhenContentIsPresent() {
        updateReportPage(makeBlueprintReport(label));

        final PagePropertiesReportMacro report = gotoReportPage();

        assertThat(report.getColumnNames(), contains("Title", "Foo"));
        assertThat(report.getColumnValues("Foo"), contains("Bar"));

        BlueprintBlankExperience blueprintBlankExperience =
                product.getPageBinder().bind(BlueprintBlankExperience.class);
        waitUntilFalse(blueprintBlankExperience.isVisible());
    }

    public static String makeBlueprintReport(final Label label) {
        return ("<ac:macro ac:name=\"detailssummary\">" +
                "<ac:parameter ac:name=\"label\">") + label.getLabel() + "</ac:parameter>" +
                "<ac:parameter ac:name=\"blueprintModuleCompleteKey\">" + blueprintModuleCompleteKey + "</ac:parameter>" +
                "<ac:parameter ac:name=\"blankTitle\">" + blankTitle + "</ac:parameter>" +
                "<ac:parameter ac:name=\"blankDescription\">" + blankDescription + "</ac:parameter>" +
                "<ac:parameter ac:name=\"createButtonLabel\">" + createButtonLabel + "</ac:parameter>" +
                "</ac:macro>";
    }
}
