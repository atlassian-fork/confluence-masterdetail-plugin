package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilEquals;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SortingOrderingLargePaginationTest extends AbstractPagePropertiesReportTest {

    @BeforeClass
    public static void create100Pages() {
        createPages(100, false);
    }

    @Test
    public void testServerRenderPagination() {
        pagination(true);
    }

    @Test
    public void testClientRenderPagination() {
        pagination(false);
    }

    private void pagination(boolean isServerMode) {
        final PagePropertiesReportMacro summary = gotoReportPage(30);
        waitUntilTrue(summary.isPaginationVisible());
        waitUntilFalse(summary.getTable().timed().hasText("contextEntityId"));

        // make sure pagination is setup correctly
        waitUntilTrue(summary.isIndexSelected(1));
        assertEquals(4, summary.getPaginationSize());
        assertThat(getTitles(summary), hasItem("Page 99"));

        // go next twice
        summary.clickNext();
        waitUntilTrue(summary.isIndexSelected(2));
        assertThat(getTitles(summary), hasItem("Page 69"));
        summary.clickNext();
        waitUntilTrue(summary.isIndexSelected(3));
        assertThat(getTitles(summary), hasItem("Page 39"));

        // go previous once
        summary.clickPrevious();
        waitUntilTrue(summary.isIndexSelected(2));
        assertThat(getTitles(summary), hasItem("Page 69"));

        // click on an index page
        summary.click(4);
        waitUntilTrue(summary.isIndexSelected(4));
        assertThat(getTitles(summary), hasItem("Page 1"));
    }

    @Test
    public void sortingTitle() {
        updateReportPage(makeReportMarkup(label, space.get(), "5", "title"));
        final PagePropertiesReportMacro summary = gotoReportPage(5);

        waitUntilTrue(summary.isPaginationVisible());
        waitUntilTrue(summary.isIndexSelected(1));
        assertEquals(7, summary.getPaginationSize());
        waitUntilEquals(5, summary.getNumberOfRows());
        assertThat(getTitles(summary), hasSize(5));
        assertThat(getTitles(summary), hasItems("Page 0", "Page 1", "Page 2", "Page 3", "Page 4"));

        summary.clickNext();
        waitUntilTrue(summary.isIndexSelected(2));
        assertThat(getTitles(summary), hasItems("Page 5", "Page 8"));

        summary.click(4);
        waitUntilTrue(summary.isIndexSelected(4));
        assertThat(getTitles(summary), hasItems("Page 15", "Page 19"));
    }
}