package it.webdriver.com.atlassian.confluence.extra.masterdetail.rest;

import com.atlassian.confluence.core.ContentEntityObject;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

// This is a copy of the same class in the plugin module. We don't want to depend on the plugin from the
// integration tests, but we also don't want to extract a common API module with Model
// classes just now.
public class DetailLine {
    public static final int COUNT_UNAVAILABLE = -1;

    @JsonIgnore
    private transient ContentEntityObject content;
    @JsonProperty
    private long id;
    @JsonProperty
    private String title;
    @JsonProperty
    private String relativeLink;
    @JsonProperty
    private String subTitle;
    @JsonProperty
    private String subRelativeLink;
    @JsonProperty
    private List<String> details;
    @JsonProperty
    private int likesCount = COUNT_UNAVAILABLE;
    @JsonProperty
    private int commentsCount = COUNT_UNAVAILABLE;

    // JAXB
    @JsonCreator
    private DetailLine() {
    }

    public DetailLine(ContentEntityObject content, List<String> details) {
        this.content = content;
        this.id = content.getId();
        this.details = details;
    }

    public ContentEntityObject getContent() {
        return content;
    }

    public List<String> getDetails() {
        return details;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public void setCommentsCount(int commentsCount) {
        this.commentsCount = commentsCount;
    }

    public String getRelativeLink() {
        return relativeLink;
    }

    public void setRelativeLink(final String relativeLink) {
        this.relativeLink = relativeLink;
    }

    public String getSubRelativeLink() {
        return subRelativeLink;
    }

    public void setSubRelativeLink(final String subRelativeLink) {
        this.subRelativeLink = subRelativeLink;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(final String subTitle) {
        this.subTitle = subTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }
}
