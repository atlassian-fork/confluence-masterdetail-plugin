package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.admin.templates.EditGlobalTemplatePage;
import com.atlassian.confluence.webdriver.pageobjects.page.admin.templates.GlobalTemplatesPage;
import com.atlassian.confluence.webdriver.pageobjects.page.admin.templates.TemplateInfo;
import com.atlassian.confluence.webdriver.pageobjects.page.content.PageTemplateWizard;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.google.common.io.CharStreams;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.io.InputStream;
import java.io.InputStreamReader;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.google.common.base.Charsets.UTF_8;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TemplateHasPagePropertiesWithVariableTest extends AbstractPagePropertiesReportTest {
    private static final String VARIABLE_NAME = "status";
    private static final String VARIABLE_VALUE = "done";

    @BeforeClass
    public static void disableWebsudo() {
        rpc.getFunctestComponent().disableWebSudo();
    }

    @AfterClass
    public static void resetWebsudo() {
        rpc.getFunctestComponent().enableWebSudo();
    }

    @Test
    public void testCreatePageFromTemplateHasPagePropertiesWithVariable() throws Exception {
        final String templateName = "template 1" + System.currentTimeMillis();
        long templateId = rpc.createTemplate(templateName, templateMarkup(), null);
        PageTemplateWizard pageTemplateWizard = product.login(user.get(), PageTemplateWizard.class, space.get(),
                templateId);
        pageTemplateWizard.enterTextVariable(VARIABLE_NAME, VARIABLE_VALUE);
        waitUntil(pageTemplateWizard.next().getEditor().getContent().getTimedHtml(), containsString(VARIABLE_VALUE));
    }

    @Test
    public void testViewTemplateHasPagePropertiesWithVariable() throws Exception {
        final String templateName = "template 2" + System.currentTimeMillis();
        rpc.createTemplate(templateName, templateMarkup(), null);

        GlobalTemplatesPage globalTempagesPage = product.login(user.get(), GlobalTemplatesPage.class);
        TemplateInfo<EditGlobalTemplatePage> templateInfo = globalTempagesPage.getTemplates().get(templateName);

        assertThat(globalTempagesPage.getTemplates(), hasEntry(is(templateName), is(notNullValue())));
        assertThat(templateInfo.viewTemplate().getContentElementText(), containsString(VARIABLE_NAME));
    }

    @Test
    public void testPreviewTemplateHasPagePropertiesWithVariable() throws Exception {
        final String templateName = "template 3" + System.currentTimeMillis();
        rpc.createTemplate(templateName, templateMarkup(), null);

        GlobalTemplatesPage globalTempagesPage = product.login(user.get(), GlobalTemplatesPage.class);
        assertThat(globalTempagesPage.getTemplates(), hasEntry(is(templateName), is(notNullValue())));

        EditGlobalTemplatePage editGlobalTemplatePage = globalTempagesPage.getTemplates().get(templateName).editTemplate();
        editGlobalTemplatePage.getEditor();
        waitUntilTrue(editGlobalTemplatePage.getEditor().getContent().htmlContains(VARIABLE_NAME));
    }

    private String templateMarkup() throws Exception {
        // The file must have variable that is the same with VARIABLE_NAME
        try (InputStream xmlStream = getClass().getClassLoader().getResourceAsStream("xml/" + "template-with-editor-format.xml")) {
            return CharStreams.toString(new InputStreamReader(xmlStream, UTF_8));
        }
    }
}