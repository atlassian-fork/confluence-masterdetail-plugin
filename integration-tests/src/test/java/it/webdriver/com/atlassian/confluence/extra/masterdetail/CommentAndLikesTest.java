package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.relations.LikeRelationDescriptor;
import com.atlassian.confluence.api.model.relations.RelationInstance;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
@RunWith(ConfluenceStatelessTestRunner.class)
@TestedProductClass(ConfluenceTestedProduct.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CommentAndLikesTest extends AbstractPagePropertiesReportTest {

    @BeforeClass
    public static void addCommentAndLike() {
        rest.relationService().create(RelationInstance.builder(user.get(),
                LikeRelationDescriptor.CONTENT_LIKE, propertiesPage).build());
        Content comment = Content.builder()
                .container(propertiesPage)
                .body("Page 1 comment", ContentRepresentation.STORAGE)
                .type(ContentType.COMMENT)
                .build();
        rest.contentService().create(comment);
    }

    @Test
    public void likesCountIsShown() {
        final boolean showCommentsCount = false;
        final boolean showLikesCount = true;
        updateReportPage(makeReportMarkup(label, showCommentsCount, showLikesCount, space.get().getKey()));

        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getLikeCounts(), is(singletonList("1")));
        assertThat(report.getCommentCounts(), hasSize(0));
    }

    @Test
    public void commentsCountIsShown() {
        final boolean showCommentsCount = true;
        final boolean showLikesCount = false;
        updateReportPage(makeReportMarkup(label, showCommentsCount, showLikesCount, space.get().getKey()));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getLikeCounts(), hasSize(0));
        assertThat(report.getCommentCounts(), is(singletonList("1")));
    }

    @Test
    public void bothCommentAndLikesCountIsShown() {
        final boolean showCommentsCount = true;
        final boolean showLikesCount = true;
        updateReportPage(makeReportMarkup(label, showCommentsCount, showLikesCount, space.get().getKey()));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getLikeCounts(), is(singletonList("1")));
        assertThat(report.getCommentCounts(), is(singletonList("1")));
    }

    @Test
    public void neitherCommentNorLikesCountIsShown() {
        final boolean showCommentsCount = false;
        final boolean showLikesCount = false;
        updateReportPage(makeReportMarkup(label, showCommentsCount, showLikesCount, space.get().getKey()));
        final PagePropertiesReportMacro report = gotoReportPage(1);
        assertThat(report.getLikeCounts(), hasSize(0));
        assertThat(report.getCommentCounts(), hasSize(0));
    }
}
