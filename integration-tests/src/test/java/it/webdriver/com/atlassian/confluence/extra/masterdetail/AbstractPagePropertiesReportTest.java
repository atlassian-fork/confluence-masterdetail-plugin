package it.webdriver.com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.api.model.content.Space;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.permissions.OperationKey;
import com.atlassian.confluence.test.api.model.person.GroupHelper;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rest.api.ConfluenceRestSession;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcClient;
import com.atlassian.confluence.test.rpc.api.ConfluenceRpcSession;
import com.atlassian.confluence.test.rpc.api.permissions.GlobalPermission;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.GroupFixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.macros.PagePropertiesReportMacro;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static com.atlassian.confluence.test.stateless.fixtures.GroupFixture.groupFixture;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.join;
import static org.apache.commons.lang3.StringUtils.substringBefore;

/**
 *
 */
public abstract class AbstractPagePropertiesReportTest {

    public static final String ALL_SPACES = "@all";

    public final static Label label = Label.builder("labelName").id("labelID").build();
    public final static List<Label> labelList = Collections.singletonList(label);

    @Inject
    public static ConfluenceRpcClient rpcClient;

    @Inject
    public static ConfluenceRestClient restClient;

    @Inject
    public static ConfluenceTestedProduct product;

    public static ConfluenceRpcSession rpc;
    public static ConfluenceRestSession rest;

    @Fixture
    public static GroupFixture group = groupFixture()
            .globalPermission(GlobalPermission.CONFLUENCE_ADMIN, GlobalPermission.SYSTEM_ADMIN)
            .namePrefix(GroupHelper.ADMINISTRATORS.getName())
            .build();

    @Fixture
    public static UserFixture user = userFixture()
            .group(group)
            .username("adult user")
            .build();

    @Fixture
    public static UserFixture userWithPageRestrictions = userFixture()
            .globalPermission(GlobalPermission.CAN_USE)
            .username("kid user")
            .build();

    @Fixture
    public static SpaceFixture space = spaceFixture()
            .permission(user, SpacePermission.ADMIN_PERMISSIONS)
            .permission(userWithPageRestrictions, SpacePermission.REGULAR_PERMISSIONS)
            .build();

    @Fixture
    public static PageFixture reportPageFixture = pageFixture()
            .title("Properties report")
            .space(space)
            .author(user)
            .build();

    @Fixture
    public static PageFixture propertiesPageFixture = pageFixture()
            .title("Page with properties")
            .space(space)
            .content(makeDetailsMarkup(null, false, "Foo", "Bar"))
            .author(user)
            .parent(reportPageFixture)
            .build();

    public static Content propertiesPage;
    public static Content reportPage;

    @BeforeClass
    public static void init() {
        rpc = rpcClient.getAdminSession();
        rest = restClient.getAdminSession();

        propertiesPage = propertiesPageFixture.get();
        reportPage = reportPageFixture.get();

        restClient.createSession(user.get()).contentRestrictionService()
                .addDirectRestrictionForSubject(propertiesPage.getId(), OperationKey.READ, user.get());
    }

    @Before
    public void setUp() {
        removeAllLabels(propertiesPageFixture);
        removeAllLabels(reportPageFixture);
        updatePropertyPage(makeDetailsMarkup(null, false, "Foo", "Bar"), label);
        updateReportPage(makeReportMarkup(label), label);
    }

    @After
    public void removeAllLabels() {
        removeAllLabels(propertiesPageFixture);
    }

    public static void removeAllLabels(final PageFixture page) {
        try {
            PageResponse<Label> labels = rest.contentLabelService().getLabels(page.get().getId(),
                    null, new SimplePageRequest(0, 50));
            for (Label label : labels.getResults()) {
                rest.contentLabelService().removeLabel(page.get().getId(), label.getLabel());
            }
        } catch (Exception e) {
            // No labels to delete
        }
    }

    /**
     * Markup for a details macro with a simple "property1", "value1" pair.
     */
    public static String makeDefaultDetailsMarkup() {
        return makeDetailsMarkup("property1", "value1");
    }

    public static String makeDetailsMarkup(String... properties) {
        return "<ac:macro ac:name=\"details\"><ac:rich-text-body>\n" +
                "   <table class=\"confluenceTable\">\n" +
                "       <tbody>\n" + serializeMetadata(buildMetadataMap(properties)) + "</tbody>" +
                "   </table>" +
                "</ac:rich-text-body></ac:macro>";
    }

    public static String makeDetailsMarkup(final String id, final Boolean hidden, final String... properties) {
        String rows = serializeMetadata(buildMetadataMap(properties));

        StringBuilder macro = new StringBuilder("<ac:macro ac:name=\"details\">");

        if (id != null) {
            macro.append("<ac:parameter ac:name=\"id\">").append(id).append("</ac:parameter>");
        }

        if (hidden != null && hidden) {
            macro.append("<ac:parameter ac:name=\"hidden\">true</ac:parameter>");
        }

        macro.append("<ac:rich-text-body>\n")
                .append("<table class=\"confluenceTable\">\n")
                .append("<tbody>\n").append(rows).append("</tbody>")
                .append("</table>")
                .append("</ac:rich-text-body></ac:macro>");

        return macro.toString();
    }

    public static Map<String, String> buildMetadataMap(String... detailOrValues) {
        Map<String, String> detailMap = new LinkedHashMap<>();
        for (Iterator<String> detailOrValue = asList(detailOrValues).iterator(); detailOrValue.hasNext(); ) {
            String property = detailOrValue.next();
            String value = detailOrValue.hasNext() ? detailOrValue.next() : EMPTY;

            detailMap.put(property, value);
        }

        return detailMap;
    }

    public static String serializeMetadata(Map<String, String> properties) {
        StringBuilder serializedBuffer = new StringBuilder();

        for (Map.Entry keyValuePair : properties.entrySet()) {
            String row = "<tr>" +
                    "    <td class=\"confluenceTd\">" + keyValuePair.getKey() + "</td>" +
                    "    <td class=\"confluenceTd\">" + keyValuePair.getValue() + "</td>" +
                    "</tr>";
            serializedBuffer.append(row);
        }

        return serializedBuffer.toString();
    }

    public static String makeReportMarkup(final Label label, final String spaceKeys, final String headings) {
        return makeReportMarkup(label, null, spaceKeys, null, headings, null, null, false, false);
    }

    public static String makeReportMarkup(final Label label, boolean showCommentsCount, boolean showLikesCount, String spaceKeys) {
        return makeReportMarkup(label, null, spaceKeys, null, null, null, null, showCommentsCount, showLikesCount);
    }

    public static String makeReportMarkup(final Label label, Space space, String pageSize, String sortBy) {
        return makeReportMarkup(label, null, space.getKey(), null, null, pageSize, sortBy);
    }

    public static String makeReportMarkup(final Label label, final String id, final String spaces,
                                          final String firstColumn, final String headings,
                                          final String pageSize, final String sortBy) {
        return makeReportMarkup(label, id, spaces, firstColumn, headings, pageSize, sortBy, false, false);
    }

    public static String makeReportMarkup(final Label label, final String id, final String spaces,
                                          final String firstColumn, final String headings,
                                          final String pageSize, final String sortBy,
                                          boolean showCommentsCount, boolean showLikesCount) {
        final StringBuilder markup = new StringBuilder();

        markup.append("<ac:macro ac:name=\"detailssummary\">")
                .append("<ac:parameter ac:name=\"label\">")
                .append(label.getLabel())
                .append("</ac:parameter>");

        if (isNotBlank(firstColumn)) {
            markup.append("<ac:parameter ac:name=\"firstcolumn\">").append(firstColumn).append("</ac:parameter>");
        }

        if (isNotBlank(spaces)) {
            markup.append("<ac:parameter ac:name=\"spaces\">").append(spaces).append("</ac:parameter>");
        }

        if (isNotBlank(id)) {
            markup.append("<ac:parameter ac:name=\"id\">").append(id).append("</ac:parameter>");
        }

        if (isNotBlank(pageSize)) {
            markup.append("<ac:parameter ac:name=\"pageSize\">").append(pageSize).append("</ac:parameter>");
        }

        if (isNotBlank(sortBy)) {
            markup.append("<ac:parameter ac:name=\"sortBy\">").append(sortBy).append("</ac:parameter>");
        }

        if (isNotBlank(headings)) {
            markup.append("<ac:parameter ac:name=\"headings\">").append(headings).append("</ac:parameter>");
        }
        if (showCommentsCount) {
            markup.append("<ac:parameter ac:name=\"showCommentsCount\">true</ac:parameter>");
        }
        if (showLikesCount) {
            markup.append("<ac:parameter ac:name=\"showLikesCount\">true</ac:parameter>");
        }

        markup.append("</ac:macro>");
        return markup.toString();
    }

    public static String createMetadata() {
        return makeDetailsMarkup("Text", "Some text",
                "Status", "<ac:structured-macro ac:name=\"status\"><ac:parameter " +
                        "ac:name=\"colour\">Grey</ac:parameter><ac:parameter " +
                        "ac:name=\"title\">GREY</ac:parameter></ac:structured-macro>");
    }

    public static String createJiraMacroMetadata() {
        int key = ThreadLocalRandom.current().nextInt(10000, 50000);
        return makeDetailsMarkup(
                "Category", "11",
                "JIRA Gadget", "<ac:structured-macro ac:name=\"jira\" ac:schema-version=\"1\" ac:macro-id=\"cbee1336-e844-4970-9ecc-109c8474709f\">" +
                        "<ac:parameter ac:name=\"server\">Atlassian JIRA</ac:parameter>" +
                        "<ac:parameter ac:name=\"serverId\">144880e9-a353-312f-9412-ed028e8166fa</ac:parameter>" +
                        "<ac:parameter ac:name=\"key\">CONFSERVER-" + String.valueOf(key) + "</ac:parameter>" +
                        "</ac:structured-macro>");
    }

    public static String createNonAsyncMacroMetadata() {
        return makeDetailsMarkup("Text", "Some text",
                "Status", "<ac:structured-macro ac:name=\"status\"><ac:parameter " +
                        "ac:name=\"colour\">Grey</ac:parameter><ac:parameter " +
                        "ac:name=\"title\">GREY</ac:parameter></ac:structured-macro>",
                "Contributors", "<ac:structured-macro " +
                        "ac:name=\"contributors\" " +
                        "ac:schema-version=\"1\"/>");
    }

    public PagePropertiesReportMacro gotoPage(Content pageToGoto, int rowCount) {
        product.viewPage(pageToGoto);
        PagePropertiesReportMacro page = product.getPageBinder().bind(PagePropertiesReportMacro.class);
        if(rowCount > 0) {
            page.waitForRowCount(rowCount);
        }
        return page;
    }

    public PagePropertiesReportMacro gotoReportPage(int rowCount) {
        rpc.getSystemComponent().flushIndexQueue();
        product.loginAndView(user.get(), reportPage);
        PagePropertiesReportMacro page = product.getPageBinder().bind(PagePropertiesReportMacro.class);
        if(rowCount > 0) {
            page.waitForRowCount(rowCount);
        }
        return page;
    }

    public PagePropertiesReportMacro gotoReportPage() {
        return gotoReportPage(0);
    }

    public void createNestedPageSummariesWithJiraIssues(final Label label, final Label parentLabel) {
        for (int i = 0; i < 3; i++) {
            createPage("Page " + i, createJiraMacroMetadata(), label);
        }

        //Parent
        createPage("Report of test-blue pages",
                makeDetailsMarkup(
                        "Priority", "2",
                        "Child Details", makeReportMarkup(label, space.get(), null, null)
                ),
                parentLabel);

        // Create grandparent page
        updateReportPage(makeReportMarkup(parentLabel, space.get().getKey(), "Title,Priority,Child Details"));
    }

    public static void createPages(int pages, boolean nonAsync) {
        createPages(pages, nonAsync, label);
    }

    public static void createPages(int pages, Label label) {
        createPages(pages, false, label);
    }

    public static void createPages(int pages, boolean nonAsync, Label label) {
        String content = nonAsync ? createNonAsyncMacroMetadata() : createMetadata();

        for (int i = 0; i < pages; i++) {
            createPage("Page " + i, content, label);
        }
    }

    public static Content createPage(final String title, String content) {
        return createPage(title, content, label);
    }

    public static Content createPage(final String title, String content, final Label label) {
        Content page = rest.contentService().create(Content.builder()
                .space(space.get())
                .title(title + ":" + System.currentTimeMillis())
                .body(content, ContentRepresentation.STORAGE)
                .type(ContentType.PAGE)
                .build());
        rest.contentLabelService().addLabels(page.getId(), Collections.singletonList(label));
        return page;
    }

    public static String makeReportMarkup(Label label, String... spaceKeys) {
        return makeReportMarkup(label, false, false, spaceKeys);
    }

    public static String makeReportMarkup(Label label, boolean showCommentsCount, boolean showLikesCount, String... spaceKeys) {
        return makeReportMarkup(label, showCommentsCount, showLikesCount, join(spaceKeys, ","));
    }

    public void updateReportPage(final String content) {
        reportPage = updatePage(content, reportPage, null);
    }

    public void updateReportPage(final String content, final Label... labels) {
        reportPage = updatePage(content, reportPage, labels);
    }

    public void updatePropertyPage(final String content) {
        propertiesPage = updatePage(content, propertiesPage, null);
    }

    public void updatePropertyPage(final String content, final Label... labels) {
        propertiesPage = updatePage(content, propertiesPage, labels);
    }
    
    public static Content updatePage(final String content, final PageFixture page) {
        return updatePage(content, page, null);
    }

    public static Content updatePage(final String content, final PageFixture page, final Label... labels) {
        return updatePage(content, page.get(), labels);
    }

    public static Content updatePage(final String content, Content page, final Label... labels) {
        if (StringUtils.isNotEmpty(content)) {
            page = rest.contentService().update(
                    Content.builder(page)
                            .body(content, ContentRepresentation.STORAGE)
                            .version(page.getVersion().nextBuilder().build())
                            .build()
            );
        }

        if (labels != null && !Arrays.asList(labels).isEmpty()) {
            rest.contentLabelService().addLabels(page.getId(), asList(labels));
        }

        return page;
    }

    public List<String> getTitles(PagePropertiesReportMacro page) {
        return page.getTitles().stream()
                .map(title -> substringBefore(title, ":"))
                .collect(Collectors.toList());
    }
}
