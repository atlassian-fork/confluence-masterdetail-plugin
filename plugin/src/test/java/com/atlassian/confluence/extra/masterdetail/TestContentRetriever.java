package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageRequest;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.PageResponseImpl;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.sal.api.message.I18nResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestContentRetriever {
    @Mock
    private CQLSearchService searchService;
    @Mock
    private ContentEntityManager contentEntityManager;
    @Mock
    private I18nResolver i18nResolver;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private DetailsSummaryMacroMetricsEvent.Builder metrics;

    private ContentRetriever contentRetriever;

    @Before
    public void setup() {
        contentRetriever = new ContentRetriever(searchService, contentEntityManager, i18nResolver);

        PageResponse<Content> pageResponse = PageResponseImpl.empty(false);
        when(searchService.searchContent(anyString(), any(SearchContext.class), any(PageRequest.class))).thenReturn(pageResponse);
    }

    @Test
    public void testSearchContextHasSpace() throws Exception {
        String spaceKey = "HAPPYNESS";

        contentRetriever.getContentWithMetaData("label = \"foo\"", false, buildSearchContext(spaceKey), metrics);

        verify(searchService, times(1)).searchContent(anyString(), argThat(contextMatcher(spaceKey)),
                any(PageRequest.class));
    }

    @Test
    public void testFinalCqlStringHasDefaultOrderByDesc() throws Exception {
        contentRetriever.getContentWithMetaData("label = \"foo\"", false, buildSearchContext("HAPPYNESS"), metrics);

        String expectedCql = "(label = \"foo\") and macro = details order by lastModified desc";
        verify(searchService, times(1)).searchContent(eq(expectedCql), any(SearchContext.class),
                any(PageRequest.class));
    }

    @Test
    public void testFinalCqlStringHasDefaultOrderByNotDesc() throws Exception {
        contentRetriever.getContentWithMetaData("label = \"foo\"", true, buildSearchContext("HAPPYNESS"), metrics);

        String expectedCql = "(label = \"foo\") and macro = details order by lastModified";
        verify(searchService, times(1)).searchContent(eq(expectedCql), any(SearchContext.class),
                any(PageRequest.class));
    }

    private SearchContext buildSearchContext(String spaceKey) {
        return SearchContext.builder().spaceKey(spaceKey).build();
    }

    private ArgumentMatcher<SearchContext> contextMatcher(final String spaceKey) {
        return item -> spaceKey.equals(item.getSpaceKey().get());
    }
}
