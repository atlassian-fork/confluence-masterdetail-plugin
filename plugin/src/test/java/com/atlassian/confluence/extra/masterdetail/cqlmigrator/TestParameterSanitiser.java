package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.atlassian.confluence.xhtml.api.MacroDefinition;
import org.junit.Test;

import java.util.List;

import static com.atlassian.confluence.extra.masterdetail.cqlmigrator.ParameterSanitiser.getParameter;
import static com.atlassian.confluence.xhtml.api.MacroDefinition.builder;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestParameterSanitiser {
    @Test
    public void testGetSpaceKeysFromCommaDelimitedString() {
        assertSpaceKeysSplit("foo,BAR, blah1234, @all", asList("foo", "BAR", "blah1234", "@all"));
    }

    @Test
    public void testGetSpaceKeysFromSemiColonDelimitedString() {
        assertSpaceKeysSplit("foo;BAR; blah1234; @all", asList("foo", "BAR", "blah1234", "@all"));
    }

    @Test
    public void testGetSpaceKeysFromSlashDelimitedString() {
        assertSpaceKeysSplit("foo/BAR/ blah1234/ @all", asList("foo", "BAR", "blah1234", "@all"));
    }

    @Test
    public void testGetSpaceKeysFromPipeDelimitedString() {
        assertSpaceKeysSplit("foo|BAR| blah1234| @all", asList("foo", "BAR", "blah1234", "@all"));
    }

    @Test
    public void testGetParameterTrims() throws Exception {
        MacroDefinition macro = builder("macroname").withParameter("foo", "  bar  ").build();
        assertThat(getParameter("foo", macro), is("bar"));
    }

    private void assertSpaceKeysSplit(String spacesString, List<String> expected) {
        List<String> result = ParameterSanitiser.getSpaceKeysFromDelimitedString(spacesString);
        assertEquals(expected, result);
    }
}