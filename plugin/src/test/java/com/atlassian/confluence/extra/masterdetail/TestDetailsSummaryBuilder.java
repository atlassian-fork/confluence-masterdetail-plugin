package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.extra.masterdetail.services.InternalPagePropertiesService;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsSummaryBuilder {
    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private InternalPagePropertiesService pagePropertiesService;

    @Mock
    private CommentManager commentManager;

    @Mock
    private LikeManager likeManager;

    private TransactionTemplate transactionTemplate = new TransactionTemplate() {
        @Override
        public <T> T execute(TransactionCallback<T> action) {
            return action.doInTransaction();
        }
    };

    @Mock
    private DetailsSummaryBuilder detailsSummaryBuilder;

    @Before
    public void setup() {
        detailsSummaryBuilder = new DetailsSummaryBuilder(xhtmlContent, commentManager, likeManager, pagePropertiesService, transactionTemplate);
    }

    @Test
    public void getPaginatedDetailLinesWithNoConcurrency() throws Exception {
        final List<ExtractedDetails> extractedDetails = getExtractedDetailses();
        when(pagePropertiesService.getDetailsFromContent(anyList(), any(), any(DetailsSummaryMacroMetricsEvent.Builder.class))).thenReturn(extractedDetails);
        DetailsSummaryParameters detailsSummaryParameters = mock(DetailsSummaryParameters.class);
        when(detailsSummaryParameters.getCurrentPage()).thenReturn(1);
        when(detailsSummaryParameters.getPageSize()).thenReturn(20);
        when(detailsSummaryParameters.getTotalPages()).thenReturn(6);
        PaginatedDetailLines paginatedDetailLines = detailsSummaryBuilder.getPaginatedDetailLines(
                detailsSummaryParameters,
                Boolean.FALSE,
                mock(DetailsSummaryMacroMetricsEvent.Builder.class),
                "DISPLAY"
        );
        assertEquals(paginatedDetailLines.getDetailLines().size(), 20);
        assertEquals(paginatedDetailLines.getDetailLines().get(0).getTitle(), "page-20");
        assertEquals(paginatedDetailLines.getDetailLines().get(19).getTitle(), "page-39");
    }

    @Test
    public void getPaginatedDetailLinesWithConcurrency() throws Exception {
        final List<ExtractedDetails> extractedDetails = getExtractedDetailses();
        when(pagePropertiesService.getDetailsFromContent(anyList(), any(), any(DetailsSummaryMacroMetricsEvent.Builder.class))).thenReturn(extractedDetails);
        DetailsSummaryParameters detailsSummaryParameters = mock(DetailsSummaryParameters.class);
        when(detailsSummaryParameters.getCurrentPage()).thenReturn(0);
        when(detailsSummaryParameters.getPageSize()).thenReturn(100);
        when(detailsSummaryParameters.getTotalPages()).thenReturn(2);
        PaginatedDetailLines paginatedDetailLines = detailsSummaryBuilder.getPaginatedDetailLines(
                detailsSummaryParameters,
                Boolean.FALSE,
                mock(DetailsSummaryMacroMetricsEvent.Builder.class),
                "DISPLAY"
        );
        assertEquals(paginatedDetailLines.getDetailLines().size(), 100);
        assertEquals(paginatedDetailLines.getDetailLines().get(0).getTitle(), "page-0");
        assertEquals(paginatedDetailLines.getDetailLines().get(99).getTitle(), "page-99");
    }

    @Test
    public void shouldHaveCurrentUserInTheadLocal() throws Exception {
        final List<ExtractedDetails> extractedDetails = getExtractedDetailses();
        when(pagePropertiesService.getDetailsFromContent(anyList(), any(), any(DetailsSummaryMacroMetricsEvent.Builder.class))).thenReturn(extractedDetails);
        DetailsSummaryParameters detailsSummaryParameters = mock(DetailsSummaryParameters.class);
        when(detailsSummaryParameters.getCurrentPage()).thenReturn(0);
        when(detailsSummaryParameters.getPageSize()).thenReturn(100);
        when(detailsSummaryParameters.getTotalPages()).thenReturn(2);
        ConfluenceUser confluenceUser = mock(ConfluenceUser.class);
        when(confluenceUser.getName()).thenReturn("userInThreadLocal");
        AuthenticatedUserThreadLocal.set(confluenceUser);
        final String masterThreadName = Thread.currentThread().getName();
        when(xhtmlContent.convertStorageToView(eq("foo"), any(ConversionContext.class))).then(invocation -> {
            //ensure the codes below are running within a different thread.
            assertNotEquals(masterThreadName, Thread.currentThread().getName());
            assertEquals("userInThreadLocal", AuthenticatedUserThreadLocal.get().getName());
            return "propertyValue";
        });
        PaginatedDetailLines paginatedDetailLines = detailsSummaryBuilder.getPaginatedDetailLines(
                detailsSummaryParameters,
                Boolean.FALSE,
                mock(DetailsSummaryMacroMetricsEvent.Builder.class),
                "DISPLAY"
        );
        assertEquals(paginatedDetailLines.getDetailLines().size(), 100);
        assertEquals(paginatedDetailLines.getDetailLines().get(0).getTitle(), "page-0");
        assertEquals(paginatedDetailLines.getDetailLines().get(99).getTitle(), "page-99");
    }


    private List<ExtractedDetails> getExtractedDetailses() {
        final List<ExtractedDetails> extractedDetails = new ArrayList<>();
        IntStream.range(0, 101).forEach(index -> {
            final Page content = new Page();
            content.setTitle("page-" + index);
            Map<String, PageProperty> pageProperties = new HashMap<String, PageProperty>();
            pageProperties.put("header1", new PageProperty("foo", "header1"));
            pageProperties.put("header2", new PageProperty("foo", "header2"));
            extractedDetails.add(new ExtractedDetails(content, pageProperties));
        });
        return extractedDetails;
    }

}
