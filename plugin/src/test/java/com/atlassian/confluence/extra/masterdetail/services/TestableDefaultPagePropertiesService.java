package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class TestableDefaultPagePropertiesService extends DefaultPagePropertiesService {
    private final PagePropertiesExtractor pagePropertiesExtractor;

    TestableDefaultPagePropertiesService(final CachingDetailsManager cachingDetailsManager,
                                         final PagePropertiesExtractor pagePropertiesExtractor) {
        super(cachingDetailsManager, pagePropertiesExtractor);
        this.pagePropertiesExtractor = pagePropertiesExtractor;
    }

    @Override
    ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> extractPropertiesFromContent(final ContentEntityObject contentEntity,
                                                                                                         final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        return pagePropertiesExtractor.extractProperties(contentEntity, metrics);
    }
}
