package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestDetailsMacroBodyHandler {
    private DetailsMacroBodyHandler handler;

    @Mock
    private ContentEntityObject forContent;
    @Mock
    private MacroDefinition macroDefinition;
    @Mock
    private DetailsSummaryMacroMetricsEvent.Builder metrics;

    @Before
    public void setUp() throws Exception {
        handler = new DetailsMacroBodyHandler(metrics);

        when(macroDefinition.getName()).thenReturn("details");
    }

    @Test
    public void badContent() throws Exception {
        String viewXhtml = "FOO";
        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals(0, details.size());
    }

    // TABMETA-30
    @Test
    public void tdElementsOnly() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <td>key1</td>" +
                "   <td>value1</td>" +
                "</tr>" +
                "<tr>" +
                "   <td>key2</td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2").getDetailStorageFormat());
    }

    // TABMETA-30
    @Test
    public void thColumn() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key1</th>" +
                "   <td>value1</td>" +
                "</tr>" +
                "<tr>" +
                "   <th>key2</th>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2").getDetailStorageFormat());
    }

    // TABMETA-30
    @Test
    public void thRow() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key1</th>" +
                "   <th>key2</th>" +
                "</tr>" +
                "<tr>" +
                "   <td>value1</td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2").getDetailStorageFormat());
    }

    // TABMETA-30
    @Test
    public void linkDetails() throws Exception {
        String bodyText = "<tbody>" +
                "<tr>" +
                "   <th>Document owner</th>" +
                "   <td><a class=\"confluence-userlink\" data-username=\"admin\" href=\"/confluence/users/viewuserprofile.action?username=admin\" data-linked-resource-id=\"360449\" data-linked-resource-type=\"userinfo\" data-base-url=\"http://localhost:8080/confluence\">Admin</a></td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(bodyText);

        // We can't be sure that the attribute order will be maintained through the DOM manipulation, so just check that
        // they're there.
        PageProperty detail = details.get("Document owner");
        String owner = detail.getDetailStorageFormat();
        assertTrue(owner.contains("class=\"confluence-userlink\""));
        assertTrue(owner.contains("data-username=\"admin\""));
        assertTrue(owner.contains(">Admin<"));
    }

    // CONFDEV-17230: escape text at column Outcome in Decision index page
    @Test
    public void displayEscapedTextIfElementContainsTextOnly() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key</th>" +
                "   <td>&lt;a&gt;This is a link&lt;/a&gt;</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("&lt;a&gt;This is a link&lt;/a&gt;", details.get("key").getDetailStorageFormat());
    }

    @Test
    public void testFormattingInProperties() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key</th>" +
                "   <td><strong>strong &lt;a&gt;text</strong></td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("<strong xmlns=\"http://www.w3.org/1999/xhtml\">strong &lt;a&gt;text</strong>", details.get("key").getDetailStorageFormat());
    }

    @Test
    public void testMacro() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key</th>" +
                "   <td><ac:macro ac:name=\"cheese\" /></td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("<ac:macro xmlns:ac=\"http://atlassian.com/content\" ac:name=\"cheese\"/>", details.get("key").getDetailStorageFormat());
    }

    @Test
    public void duplicateColumns() {
        String xhtml = "<tbody>" +
                "<tr>" +
                "   <th>key</th>" +
                "   <td>foo</td>" +
                "</tr>" +
                "<tr>" +
                "   <th>key</th>" +
                "   <td>bar</td>" +
                "</tr>" +
                "</tbody>";
        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(xhtml);
        assertEquals("foo", details.get("key").getDetailStorageFormat());
    }

    @Test
    public void multipleDetails() {
        String id = "foo";
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <th>key1</th>" +
                "   <th>key2</th>" +
                "</tr>" +
                "<tr>" +
                "   <td>value1</td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        handleMacroDefinition(viewXhtml, id);

        String viewXhtml2 = "<tbody>" +
                "<tr>" +
                "   <th>one</th>" +
                "</tr>" +
                "<tr>" +
                "   <td>two</td>" +
                "</tr>" +
                "</tbody>";
        handleMacroDefinition(viewXhtml2, null);

        String viewXhtml3 = "<tbody>" +
                "<tr>" +
                "   <th>three</th>" +
                "</tr>" +
                "<tr>" +
                "   <td>four</td>" +
                "</tr>" +
                "</tbody>";
        handleMacroDefinition(viewXhtml3, null);

        ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> result = handler.getDetails();
        assertEquals(2, result.size());

        ImmutableMap<String, PageProperty> details = result.get(id).get(0);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2").getDetailStorageFormat());

        ImmutableMap<String, PageProperty> details2 = result.get("").get(0);
        assertEquals("two", details2.get("one").getDetailStorageFormat());
        ImmutableMap<String, PageProperty> details3 = result.get("").get(1);
        assertEquals("four", details3.get("three").getDetailStorageFormat());
    }

    @Test // CONF-31518
    public void nbspStripped() throws Exception {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <td>key1&#160;</td>" +
                "   <td>value1</td>" +
                "</tr>" +
                "<tr>" +
                "   <td>&#160;key2</td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2").getDetailStorageFormat());
    }

    @Test //CONF-32367
    public void headingTagsIgnored() {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <td><span>key1</span></td>" +
                "   <td>value1</td>" +
                "</tr>" +
                "<tr>" +
                "   <td><p>key2</p><strong>in bold</strong></td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2in bold").getDetailStorageFormat());
    }

    @Test //CONF-32367
    public void nestedHeadingTagsIgnored() {
        String viewXhtml = "<tbody>" +
                "<tr>" +
                "   <td><span><strong>key1</strong></span></td>" +
                "   <td>value1</td>" +
                "</tr>" +
                "<tr>" +
                "   <td><p>key2 <strong>in bold</strong></p></td>" +
                "   <td>value2</td>" +
                "</tr>" +
                "</tbody>";

        Map<String, PageProperty> details = handleMacroAndGetDetailLineMap(viewXhtml);
        assertEquals("value1", details.get("key1").getDetailStorageFormat());
        assertEquals("value2", details.get("key2 in bold").getDetailStorageFormat());
    }

    private void handleMacroDefinition(String body, String id) {
        MacroDefinition macroDefinition2 = Mockito.mock(MacroDefinition.class);
        when(macroDefinition2.getName()).thenReturn("details");
        when(macroDefinition2.getBodyText()).thenReturn(body);
        if (id != null)
            when(macroDefinition2.getParameter(DetailsSummaryMacro.PARAM_ID)).thenReturn(id);

        handler.handle(macroDefinition2);
    }

    private Map<String, PageProperty> handleMacroAndGetDetailLineMap(String viewXhtml) {
        when(macroDefinition.getBodyText()).thenReturn(viewXhtml);
        handler.handle(macroDefinition);
        return handler.getDetails("").get(0);
    }
}
