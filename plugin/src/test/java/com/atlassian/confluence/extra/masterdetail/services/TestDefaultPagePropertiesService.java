package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.definition.PlainTextMacroBody;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.confluence.extra.masterdetail.ExtractedDetails;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroInstance;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroReport;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.MacroDefinitionMarshallingStrategy;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.util.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultPagePropertiesService {
    @Mock
    private CachingDetailsManager cachingDetailsManager;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private DetailsSummaryMacroMetricsEvent.Builder metrics;

    @Test
    public void testGetPagePropertiesMacroReportWithoutId() throws Exception {
        XhtmlContent xhtmlContent = mockDetailContentWithoutId();
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);
        PagePropertiesMacroReport report = pagePropertiesService.getReportFromContent(mock(AbstractPage.class));
        List<PagePropertiesMacroInstance> macroInstances = report.getMacroInstancesForId("");
        assertThat(macroInstances, hasSize(1));
        verifyPagePropertyReportForMacro(macroInstances.get(0));
    }

    @Test
    public void testGetPagePropertiesMacroReportWithId() throws Exception {
        XhtmlContent xhtmlContent = mockDetailContentWithId();
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);
        PagePropertiesMacroReport report = pagePropertiesService.getReportFromContent(mock(AbstractPage.class));
        List<PagePropertiesMacroInstance> macroInstances = report.getMacroInstancesForId("999");
        assertThat(macroInstances, hasSize(1));
        verifyPagePropertyReportForMacroHasId(macroInstances.get(0));
    }

    @Test
    public void testGetDetailsFromNoPagePropertiesContent() throws Exception {
        MacroDefinition infoMacroDefinition = createMacroDefinition("info", new HashMap<>(),
                "<p>This is information</p>");
        XhtmlContent xhtmlContent = mockXhtmlContent(infoMacroDefinition);
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);
        PagePropertiesMacroReport report = pagePropertiesService.getReportFromContent(mock(AbstractPage.class));
        assertThat(report.getAllMacroInstances(), hasSize(0));
    }

    private void verifyPagePropertyReportForMacro(PagePropertiesMacroInstance pagePropertiesMacroInstance) {
        Map<String, PageProperty> pagePropertyReportRow = pagePropertiesMacroInstance.getPagePropertyReportRow();
        assertEquals("Two PagePropertyRow should be extracted.", 2, pagePropertyReportRow.size());
        verifyPagePropery(pagePropertyReportRow.get("Status 2"), "Status 2", "Good status 2");
        verifyPagePropery(pagePropertyReportRow.get("Stakeholder 2"), "Stakeholder 2", "Stakeholder person 2");
    }

    private void verifyPagePropertyReportForMacroHasId(PagePropertiesMacroInstance pagePropertiesMacroInstance) {
        Map<String, PageProperty> pagePropertyReportRow = pagePropertiesMacroInstance.getPagePropertyReportRow();
        assertEquals("Three PagePropertyRow should be extracted.", 3, pagePropertyReportRow.size());
        verifyPagePropery(pagePropertyReportRow.get("Status 1"), "Status 1", "Good status 1");
        verifyPagePropery(pagePropertyReportRow.get("Stakeholder 1"), "Stakeholder 1", "Stakeholder person 1");
        verifyPagePropery(pagePropertyReportRow.get("Description 1"), "Description 1", "Description 1");
    }

    private void verifyPagePropery(PageProperty pageProperty, String headingStorage, String detailStorage) {
        assertEquals(headingStorage, pageProperty.getHeadingStorageFormat());
        assertEquals(detailStorage, pageProperty.getDetailStorageFormat());
    }

    @Test
    public void testGetDetailsFromDetailContentList() throws Exception {
        XhtmlContent xhtmlContent = mockDetailContents();
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);

        List<ExtractedDetails> extractedDetailsList = pagePropertiesService.getDetailsFromContent(getDetailsContents(),
                "", metrics);
        assertThat("One ExtractedDetails should be extracted.", extractedDetailsList, hasSize(1));
        Map<String, PageProperty> mapDetails = extractedDetailsList.get(0).getDetails();
        // gets all details from 2 Page properties macros from content
        assertEquals("Five PageProperties should be extracted.", 5, mapDetails.size());
        verifyExtractedDetailFromDetailMacro(mapDetails);
        verifyExtractedDetailFromDetailMacroHasId(mapDetails);
    }

    @Test
    public void testGetDetailsFromDetailContentListWithDetailsId() throws Exception {
        XhtmlContent xhtmlContent = mockDetailContents();
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);

        List<ExtractedDetails> extractedDetailsList = pagePropertiesService.getDetailsFromContent(getDetailsContents(),
                "999", metrics);
        assertThat("One ExtractedDetails should be extracted.", extractedDetailsList, hasSize(1));
        Map<String, PageProperty> mapDetails = extractedDetailsList.get(0).getDetails();
        // only gets details from Page properties macro has detailsId = 999
        assertEquals("Three PageProperties should be extracted.", 3, mapDetails.size());
        verifyExtractedDetailFromDetailMacroHasId(mapDetails);
    }

    @Test
    public void testGetDetailsFromNoPagePropertiesContentList() throws Exception {
        MacroDefinition infoMacroDefinition = createMacroDefinition("info", new HashMap<>(),
                "<p>This is information</p>");
        XhtmlContent xhtmlContent = mockXhtmlContent(infoMacroDefinition);
        DefaultPagePropertiesService pagePropertiesService = getMockService(xhtmlContent);

        List<ExtractedDetails> extractedDetailsList = pagePropertiesService.getDetailsFromContent(getDetailsContents(),
                "", metrics);
        assertThat("Should be an empty ExtractedDetails list extracted from page doesn't have Page properties macro.",
                extractedDetailsList,
                empty());
    }

    private void verifyExtractedDetailFromDetailMacro(Map<String, PageProperty> mapDetails) {
        verifyPageProperty(mapDetails, "Status 2", "Status 2", "Good status 2");
        verifyPageProperty(mapDetails, "Stakeholder 2", "Stakeholder 2", "Stakeholder person 2");
    }

    private void verifyExtractedDetailFromDetailMacroHasId(Map<String, PageProperty> mapDetails) {
        verifyPageProperty(mapDetails, "Status 1", "Status 1", "Good status 1");
        verifyPageProperty(mapDetails, "Stakeholder 1", "Stakeholder 1", "Stakeholder person 1");
        verifyPageProperty(mapDetails, "Description 1", "Description 1", "Description 1");
    }

    private void verifyPageProperty(Map<String, PageProperty> mapDetails, String headingKey, String headingStorage,
                                    String detailStorage) {
        PageProperty pageProperty = mapDetails.get(headingKey);
        verifyPagePropery(pageProperty, headingStorage, detailStorage);
    }

    private XhtmlContent mockDetailContentWithoutId() throws Exception {
        MacroDefinition detailMacroDefinition = mockDetailMacroWithoutId();

        return mockXhtmlContent(detailMacroDefinition);
    }

    private XhtmlContent mockDetailContentWithId() throws Exception {
        MacroDefinition detailWithIdMacroDefinition = mockDetailMacroWithId();

        return mockXhtmlContent(detailWithIdMacroDefinition);
    }

    private XhtmlContent mockDetailContents() throws Exception {
        MacroDefinition detailWithIdMacroDefinition = mockDetailMacroWithId();
        MacroDefinition detailMacroDefinition = mockDetailMacroWithoutId();

        return mockXhtmlContent(detailWithIdMacroDefinition, detailMacroDefinition);
    }

    private MacroDefinition mockDetailMacroWithId() throws Exception {
        // xhtml content for Page properties macro has detailsId param = 999
        String detailWithIdMacroBody = FileUtils.getResourceContent("details-macro-with-id.xml");
        HashMap<String, String> params = new HashMap<>();
        params.put("id", "999");
        return createDetailsMacroDefinition(params, detailWithIdMacroBody);
    }

    private MacroDefinition mockDetailMacroWithoutId() throws Exception {
        // xhtml content for Page properties macro without detailsId
        String detailMacroBody = FileUtils.getResourceContent("details-macro.xml");
        return createDetailsMacroDefinition(new HashMap<>(), detailMacroBody);
    }

    private XhtmlContent mockXhtmlContent(final MacroDefinition... definitions) throws Exception {
        XhtmlContent xhtmlContent = mock(XhtmlContent.class);
        doAnswer(inv -> {
            MacroDefinitionHandler handler = (MacroDefinitionHandler) inv.getArguments()[2];
            for (MacroDefinition definition : definitions) {
                handler.handle(definition);
            }
            return null;
        }).when(xhtmlContent).handleMacroDefinitions(any(), any(ConversionContext.class), any(
                MacroDefinitionHandler.class), any(MacroDefinitionMarshallingStrategy.MARSHALL_MACRO.getClass()));
        return xhtmlContent;
    }

    private MacroDefinition createDetailsMacroDefinition(Map<String, String> parameters, String detailMacroBody) {
        return createMacroDefinition("details", parameters, detailMacroBody);
    }

    private MacroDefinition createMacroDefinition(String name, Map<String, String> parameters, String macroBody) {
        return MacroDefinition.builder(name)
                .withParameters(parameters)
                .withMacroBody(new PlainTextMacroBody(macroBody))
                .build();
    }

    private List<ContentEntityObject> getDetailsContents() {
        List<ContentEntityObject> contents = new ArrayList<>();
        AbstractPage infoPage = mock(AbstractPage.class);
        contents.add(infoPage);
        return contents;
    }

    private DefaultPagePropertiesService getMockService(XhtmlContent mockXhtmlContent) {
        PagePropertiesExtractor pagePropertiesExtractor = new PagePropertiesExtractor(mockXhtmlContent);
        return new TestableDefaultPagePropertiesService(cachingDetailsManager, pagePropertiesExtractor);
    }
}