package com.atlassian.confluence.plugins.pageproperties.api.model;

import com.atlassian.confluence.core.ContentEntityObject;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Simple object to represent the extracted PageProperty report for all Page Properties macros in a page {@link
 * ContentEntityObject}, each Page Properties macro will be extracted into a {@link PagePropertiesMacroInstance} and map
 * by Page Properties macro ID and {@link List} of {@link PagePropertiesMacroInstance} because of we could have multiple
 * Page Property macros have same ID on one page.
 */
public class PagePropertiesMacroReport {
    private final Map<String, List<PagePropertiesMacroInstance>> macroInstancesMap;

    public PagePropertiesMacroReport(Map<String, List<PagePropertiesMacroInstance>> macroInstancesMap) {
        this.macroInstancesMap = ImmutableMap.copyOf(macroInstancesMap);
    }

    /**
     * Gets all the {@link PagePropertiesMacroInstance} has Page Properties macro ID was given.
     *
     * @param pagePropertiesMacroId ID of Page Properties macro need to get.
     * @return {link List} of {@link PagePropertiesMacroInstance}, empty if doesn't have.
     */
    public List<PagePropertiesMacroInstance> getMacroInstancesForId(String pagePropertiesMacroId) {
        return macroInstancesMap.get(pagePropertiesMacroId);
    }

    /**
     * Gets all the {@link PagePropertiesMacroInstance} on the page.
     *
     * @return {link List} of {@link PagePropertiesMacroInstance}, empty if doesn't have.
     */
    public List<PagePropertiesMacroInstance> getAllMacroInstances() {
        return macroInstancesMap.values().stream().flatMap(List::stream).collect(toList());
    }
}