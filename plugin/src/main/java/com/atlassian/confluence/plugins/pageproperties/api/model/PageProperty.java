package com.atlassian.confluence.plugins.pageproperties.api.model;

import java.io.Serializable;

/**
 * Simple object to represent the extracted details for an object from a table in Page properties macro. Note that
 * headings will be just plain text and not include html markup.
 */
public class PageProperty implements Serializable {
    private final String detailStorageFormat;
    private final String headingStorageFormat;

    public PageProperty(String detailStorageFormat, String headingStorageFormat) {
        this.detailStorageFormat = detailStorageFormat;
        this.headingStorageFormat = headingStorageFormat;
    }

    public String getDetailStorageFormat() {
        return detailStorageFormat;
    }

    public String getHeadingStorageFormat() {
        return headingStorageFormat;
    }
}
