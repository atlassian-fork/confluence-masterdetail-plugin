package com.atlassian.confluence.plugins.pageproperties.api.service;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroInstance;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroReport;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;

import java.util.List;

/**
 * This service responsible for extracting all page properties for given content {@link ContentEntityObject}. Tables in
 * Page properties macros are converted into a {@link List} of {@link PagePropertiesMacroReport} with Page Properties ID and
 * all table rows inside Page Properties macro becomes {@link PagePropertiesMacroInstance} keyed by heading text to {@link PageProperty}.
 */
public interface PagePropertiesService {
    /**
     * Get {@link PagePropertiesMacroReport} (extracted from tables in Page properties macros) for given content.
     *
     * @param contentEntity the {@link ContentEntityObject} need to extract the detail {@link PageProperty}
     * @return the {@link PagePropertiesMacroReport} extracting from all Page Properties macros mapped by ID.
     */
    PagePropertiesMacroReport getReportFromContent(final ContentEntityObject contentEntity);
}
