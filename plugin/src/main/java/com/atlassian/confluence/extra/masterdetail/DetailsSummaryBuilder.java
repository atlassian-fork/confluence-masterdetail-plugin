package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.bonnie.Searchable;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.Addressable;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.extra.masterdetail.entities.DetailLine;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.extra.masterdetail.services.InternalPagePropertiesService;
import com.atlassian.confluence.like.LikeManager;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.fugue.Either;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.xml.stream.XMLStreamException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.atlassian.fugue.Either.left;
import static com.atlassian.fugue.Either.right;
import static com.atlassian.fugue.Functions.fold;
import static com.google.common.collect.Collections2.transform;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Lists.partition;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static java.util.Collections.sort;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

@Component
public class DetailsSummaryBuilder {
    private static final Logger log = LoggerFactory.getLogger(DetailsSummaryBuilder.class);
    private static final Integer ROWS_PER_PARALLEL_RENDER = 50;
    private static final Long RENDER_TIMEOUT_SECONDS = 60L;
    private static final int THREAD_POOL_SIZE = Integer.getInteger("confluence.masterdetails.thread.pool.size", 4);
    private static ExecutorService pool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    private final XhtmlContent xhtmlContent;
    private final CommentManager commentManager;
    private final LikeManager likeManager;
    private final InternalPagePropertiesService pagePropertiesService;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public DetailsSummaryBuilder(final XhtmlContent xhtmlContent,
                                 @ComponentImport final CommentManager commentManager,
                                 @ComponentImport final LikeManager likeManager,
                                 final InternalPagePropertiesService pagePropertiesService,
                                 @ComponentImport final TransactionTemplate transactionTemplate) {
        this.xhtmlContent = xhtmlContent;
        this.commentManager = commentManager;
        this.likeManager = likeManager;
        this.pagePropertiesService = pagePropertiesService;
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * Calculates the unique set of headings based on all the details provided
     *
     * @param detailLines a collection of ExtractedDetails to calculate the headings from
     * @return a list of DetailsHeadings in alphabetical order
     */
    private List<DetailsHeading> buildHeadingsFromDetails(Collection<ExtractedDetails> detailLines) {
        final TreeMap<String, String> headers = new TreeMap<>();
        for (ExtractedDetails detailLine : detailLines) {
            if (!detailLine.getDetails().isEmpty()) {
                for (String headingText : detailLine.getDetails().keySet()) {
                    if (headers.get(headingText) == null) {
                        try {
                            String renderedValue = headingText;
                            DefaultConversionContext context = new DefaultConversionContext(detailLine.getContent().toPageContext());
                            String headingStorageFormat = detailLine.getDetails().get(headingText).getHeadingStorageFormat();

                            if (isNotBlank(headingStorageFormat)) {
                                renderedValue = xhtmlContent.convertStorageToView(headingStorageFormat, context);
                            }

                            headers.put(headingText, renderedValue);
                        } catch (XMLStreamException | XhtmlException e) {
                            log.error("Cannot render xhtml content for heading in page properties macro:" + headingText, e);
                        }
                    }
                }
            }
        }

        return newArrayList(transform(headers.entrySet(), entry -> new DetailsHeading(entry.getKey(), entry.getValue())));
    }

    /**
     * Note that because it is user input, the rendered heading must be escaped
     *
     * @param headingsString the user input heading string
     * @return a list of DetailsHeadings in order provided by the headingString
     */
    private static List<DetailsHeading> buildHeadingsFromParameter(String headingsString) {
        final Set<DetailsHeading> headings = newLinkedHashSet();
        final StrTokenizer tokenizer = StrTokenizer.getCSVInstance(headingsString);
        while (tokenizer.hasNext()) {
            final String heading = tokenizer.nextToken();
            if (isNotBlank(heading)) {
                String escapedHeading = escapeHtml4(heading);
                headings.add(new DetailsHeading(escapedHeading, escapedHeading));
            }
        }
        return new ArrayList<>(headings);
    }

    private void fillWithCountsAndLink(final List<DetailLine> detailLines, final boolean countComments, final boolean countLikes, final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        final Map<Searchable, Integer> likesCounts = countLikes ? countLikes(detailLines, metrics) : null;
        final Map<Searchable, Integer> commentsCounts = countComments ? countComments(detailLines, metrics) : null;

        for (final DetailLine detailLine : detailLines) {
            fillLineTitleLink(detailLine);
            if (commentsCounts != null) {
                final Integer likes = commentsCounts.get(detailLine.getContent());
                detailLine.setCommentsCount(likes);
            }
            if (likesCounts != null) {
                final Integer comments = likesCounts.get(detailLine.getContent());
                detailLine.setLikesCount(comments);
            }
        }
    }

    private Map<Searchable, Integer> countLikes(final List<DetailLine> detailLines, final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        final Map<Searchable, Integer> likesCounts;
        metrics.summaryTableCountLikesStart();
        likesCounts = likeManager.countLikes(searchablesFrom(detailLines));
        metrics.summaryTableCountLikesFinish(count(likesCounts.values()));
        return likesCounts;
    }

    private Map<Searchable, Integer> countComments(final List<DetailLine> detailLines, final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        final Map<Searchable, Integer> commentsCounts;
        metrics.summaryTableCountCommentsStart();
        commentsCounts = commentManager.countComments(searchablesFrom(detailLines));
        metrics.summaryTableCountCommentsFinish(count(commentsCounts.values()));
        return commentsCounts;
    }

    private static int count(Iterable<Integer> numbers) {
        return fold((arg1, arg2) -> arg1 + arg2, 0, numbers);
    }

    private static Collection<Searchable> searchablesFrom(final Collection<DetailLine> detailLines) {
        return transform(detailLines, DetailLine::getContent);
    }

    private static void fillLineTitleLink(final DetailLine detailLine) {
        final Addressable content = detailLine.getContent();
        final String type = content.getType();

        if (Comment.CONTENT_TYPE.equals(type)) {
            Comment comment = (Comment) content;

            ContentEntityObject owner = comment.getContainer();

            detailLine.setTitle(owner.getDisplayTitle());
            detailLine.setSubTitle(comment.getDisplayTitle());

            detailLine.setRelativeLink(owner.getUrlPath());
            detailLine.setSubRelativeLink(content.getUrlPath());
        } else {
            detailLine.setTitle(content.getDisplayTitle());
            detailLine.setRelativeLink(content.getUrlPath());
        }
    }

    /*
         * If a comma-separated list of headings is present as a "headings" parameters, then split and sanitize that list.
         * Else walk all detail lines and return the keys. Note it is important to keep ordering of the headings so it
         * must return a list.
         */
    private List<DetailsHeading> getHeadings(@Nullable String headingsString, final Collection<ExtractedDetails> detailLines, final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        metrics.summaryTableHeadersBuildStart();
        final List<DetailsHeading> headers;
        if (isNotBlank(headingsString)) {
            headers = buildHeadingsFromParameter(headingsString);
        } else {
            headers = buildHeadingsFromDetails(detailLines);
        }
        metrics.summaryTableHeadersBuildFinish(headers.size());
        return headers;
    }

    public PaginatedDetailLines getPaginatedDetailLines(
            final DetailsSummaryParameters params,
            final boolean requireAsyncRenderSafe,
            final DetailsSummaryMacroMetricsEvent.Builder metrics,
            final String outputType) {
        // First bailout, just in case your requested page is *way* too large (content.size() is >= final size)
        params.checkPageBounds();

        final List<ExtractedDetails> extractedDetails = pagePropertiesService.getDetailsFromContent(params.getContent(),
                params.getId(), metrics);
        if (extractedDetails.isEmpty()) {
            return PaginatedDetailLines.empty();
        }

        if (isNotBlank(params.getSortBy())) {
            sort(extractedDetails, new ExtractedDetailsComparator(escapeHtml4(params.getSortBy()), params.isReverseSort()));
        }

        final List<DetailsHeading> headings = getHeadings(params.getHeadingsString(), extractedDetails, metrics);
        final List<String> renderedHeadings = Lists.transform(headings, DetailsHeading::getRenderedHeading);

        final List<List<ExtractedDetails>> pagedDetails = partition(extractedDetails, params.getPageSize());
        final List<ExtractedDetails> detailsForCurrentPage = pagedDetails.get(params.getCurrentPage());

        final boolean isPaginated = params.getTotalPages() > 1;

        return renderDetailRows(headings, detailsForCurrentPage, requireAsyncRenderSafe, metrics, isPaginated, outputType).fold(
                renderError -> renderErrorResult(renderError, renderedHeadings),
                renderedLines -> {
                    fillWithCountsAndLink(renderedLines, params.isCountComments(), params.isCountLikes(), metrics);
                    return new PaginatedDetailLines(renderedHeadings, renderedLines, true);
                }
        );
    }

    private static PaginatedDetailLines renderErrorResult(@Nullable RenderError input, List<String> headings) {
        final boolean asyncRenderSafe = input != RenderError.NON_ASYNC_RENDER_SAFE;
        return new PaginatedDetailLines(headings, Collections.<DetailLine>emptyList(), asyncRenderSafe);
    }

    private Either<RenderError, List<DetailLine>> renderDetailRows(final List<DetailsHeading> headings,
                                                                   final List<ExtractedDetails> pagedDetailLines,
                                                                   boolean requireAsyncRenderSafe,
                                                                   final DetailsSummaryMacroMetricsEvent.Builder metrics,
                                                                   boolean isPaginated,
                                                                   String outputType) {
        metrics.summaryTableBodyBuildStart();
        final List<DetailLine> renderedLines = new ArrayList<>();
        if (pagedDetailLines.isEmpty()) {
            return right(renderedLines);
        }

        final List<CompletableFuture<Either<RenderError, List<DetailLine>>>> futures = new ArrayList<>();
        final ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        partition(pagedDetailLines, ROWS_PER_PARALLEL_RENDER).forEach(lines ->
                futures.add(CompletableFuture.supplyAsync(() -> renderDetailRowsWithTransaction(headings, lines, requireAsyncRenderSafe, isPaginated, outputType, currentUser), pool))
        );
        //keep the sort order by iterating in the order of partition.
        for (CompletableFuture<Either<RenderError, List<DetailLine>>> future : futures) {
            Either<RenderError, List<DetailLine>> futureResult = null;
            try {
                futureResult = future.get(RENDER_TIMEOUT_SECONDS, TimeUnit.SECONDS);
                if (futureResult.isLeft()) {
                    return left(futureResult.left().get());
                } else {
                    renderedLines.addAll(futureResult.right().get());
                }
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                log.warn("Timeout when rendering the page properties report", e);
                future.cancel(true);
            }
        }
        ;

        metrics.summaryTableBodyBuildFinish(renderedLines.size());
        return right(renderedLines);
    }

    private Either<RenderError, List<DetailLine>> renderDetailRowsWithTransaction(final List<DetailsHeading> headings,
                                                                                  final List<ExtractedDetails> pagedDetailLines,
                                                                                  final boolean requireAsyncRenderSafe,
                                                                                  final boolean isPaginated,
                                                                                  final String outputType,
                                                                                  final ConfluenceUser currentUser) {
        try {
            AuthenticatedUserThreadLocal.set(currentUser);
            return transactionTemplate.execute(() -> renderDetailRows(headings, pagedDetailLines, requireAsyncRenderSafe, isPaginated, outputType));
        } catch (Exception e) {
            log.error("Transaction error occurred when rendering the page property report.", e);
            return right(new ArrayList<>());
        }
    }

    private Either<RenderError, List<DetailLine>> renderDetailRows(final List<DetailsHeading> headings, final List<ExtractedDetails> pagedDetailLines, boolean requireAsyncRenderSafe, boolean isPaginated, String outputType) {
        final List<DetailLine> renderedLines = new ArrayList<>();

        for (ExtractedDetails line : pagedDetailLines) {
            final List<String> cells = new ArrayList<>(headings.size());
            final ContentEntityObject lineContent = line.getContent();
            final Map<String, PageProperty> lineDetails = line.getDetails();
            final PageContext pageContext = lineContent.toPageContext();
            pageContext.setOutputType(outputType);

            // CONF-40995 in case the master detail summary has more than 1 page then disable SERVER render mode
            // and force rendering content in PREVIEW mode. All macros "should" return non-async content when PREVIEW mode is given.
            // Also don't override in case of word or pdf export ...
            if (isPaginated /*&& ConversionContextOutputType.DISPLAY.value().equals(pageContext.getOutputType())*/) {
                pageContext.setOutputType(ConversionContextOutputType.PREVIEW.value());
            }

            final DefaultConversionContext subContext = new DefaultConversionContext(pageContext);

            for (DetailsHeading heading : headings) {
                PageProperty pageProperty = lineDetails.get(heading.getHeading());
                boolean added = false;
                if (pageProperty != null && isNotBlank(pageProperty.getDetailStorageFormat())) {
                    final String value = pageProperty.getDetailStorageFormat();
                    try {
                        final String renderedValue = xhtmlContent.convertStorageToView(value, subContext);

                        if (!subContext.isAsyncRenderSafe()
                                && requireAsyncRenderSafe
                                && !isPaginated) {
                            return left(RenderError.NON_ASYNC_RENDER_SAFE);
                        }
                        cells.add(renderedValue);
                        added = true;
                    } catch (XMLStreamException | XhtmlException e) {
                        log.error("Cannot render xhtml content for page properties macro:" + value, e);
                    }
                }
                if (!added) {
                    cells.add("");
                }
            }
            renderedLines.add(new DetailLine(lineContent, cells));
        }
        return right(renderedLines);
    }

    private enum RenderError {
        NON_ASYNC_RENDER_SAFE
    }
}
