package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.google.common.collect.Lists;

import java.util.List;

import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.split;
import static org.apache.commons.lang3.StringUtils.trim;

/**
 * Cleans up parameters being converted by the migrator.
 */
class ParameterSanitiser {
    static List<String> getSpaceKeysFromDelimitedString(final String spacesString) {
        final String spacesParameter = trim(spacesString);
        if (isBlank(spacesParameter)) {
            return Lists.newArrayList();
        }
        // Split by a range of common delimiters
        return asList(split(spacesParameter, ",;/| "));
    }

    public static String getParameter(String paramKey, MacroDefinition macro) {
        String value = macro.getParameter(paramKey);
        return value != null ? value.trim() : null;
    }
}
