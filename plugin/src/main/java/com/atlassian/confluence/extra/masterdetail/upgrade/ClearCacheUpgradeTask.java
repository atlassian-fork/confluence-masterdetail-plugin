package com.atlassian.confluence.extra.masterdetail.upgrade;

import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@ExportAsService(PluginUpgradeTask.class)
public class ClearCacheUpgradeTask implements PluginUpgradeTask {
    private final CachingDetailsManager cachingDetailsManager;

    @Autowired
    public ClearCacheUpgradeTask(CachingDetailsManager cachingDetailsManager) {
        this.cachingDetailsManager = cachingDetailsManager;
    }

    @Override
    public int getBuildNumber() {
        return 2; //update this number whenever the cache structure changes
    }

    @Override
    public String getShortDescription() {
        return "Clears the page properties cache";
    }

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        cachingDetailsManager.clearCache();
        return null;
    }

    @Override
    public String getPluginKey() {
        return "confluence.extra.masterdetail";
    }
}
