package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.event.events.content.Contented;
import com.atlassian.confluence.event.events.types.ConfluenceEntityUpdated;
import com.atlassian.confluence.event.events.types.Removed;
import com.atlassian.confluence.event.events.types.Trashed;
import com.atlassian.confluence.event.events.types.Updated;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.util.concurrent.Lazy;
import com.atlassian.vcache.ExternalCacheSettingsBuilder;
import com.atlassian.vcache.Marshaller;
import com.atlassian.vcache.StableReadExternalCache;
import com.atlassian.vcache.VCacheFactory;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

import static com.atlassian.vcache.VCacheUtils.join;
import static com.atlassian.vcache.marshallers.MarshallerFactory.serializableMarshaller;

@Component
public class CachingDetailsManager implements InitializingBean, DisposableBean {
    private static final String CACHE_NAME = CachingDetailsManager.class.getName();

    private final com.atlassian.util.concurrent.Supplier<StableReadExternalCache<ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>>>> cacheRef;
    private final EventListenerRegistrar eventListenerRegistrar;

    @Autowired
    public CachingDetailsManager(@ComponentImport VCacheFactory cacheFactory, @ComponentImport @Qualifier("eventListenerRegistrar") EventListenerRegistrar eventListenerRegistrar) {
        this.eventListenerRegistrar = eventListenerRegistrar;
        this.cacheRef = Lazy.supplier(() -> createCache(cacheFactory));
    }

    @SuppressWarnings("unchecked")
    private StableReadExternalCache<ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>>> createCache(VCacheFactory cacheFactory) {
        return cacheFactory.getStableReadExternalCache(
                CACHE_NAME,
                (Marshaller) serializableMarshaller(ImmutableMap.class),
                new ExternalCacheSettingsBuilder().build()
        );
    }

    /**
     * Gets all the properties for a given page id
     */
    @SuppressWarnings("unchecked")
    public ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> get(final long pageId, Supplier<ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>>> loader) {
        return join(cacheRef.get().get(Long.toString(pageId), loader));
    }

    @EventListener
    public void onContentUpdated(Updated event) {
        invalidateCacheOnContentEvent(event);
    }

    @EventListener
    public void onContentRemoved(Removed event) {
        invalidateCacheOnContentEvent(event);
    }

    @EventListener
    public void onContentTrashed(Trashed event) {
        invalidateCacheOnContentEvent(event);
    }

    @EventListener
    public void onConfluenceEntityUpdated(ConfluenceEntityUpdated event) {
        invalidateCacheOnContentEvent(event);
    }

    @EventListener
    public void onPluginEnable(PluginEnabledEvent event) {
        // Avoids a classcast exception where the version of the class in the cache is stale after this plugin
        // is updated.
        if (event.getPlugin().getKey().equals("confluence.extra.masterdetail"))
            clearCache();
    }

    private void invalidateCacheOnContentEvent(final Object event) {
        if (event instanceof Contented)
            join(cacheRef.get().remove(((Contented) event).getContent().getIdAsString()));
    }

    public void clearCache() {
        join(cacheRef.get().removeAll());
    }

    @Override
    public void destroy() throws Exception {
        eventListenerRegistrar.unregister(this);
        clearCache();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventListenerRegistrar.register(this);
        clearCache();
    }
}
