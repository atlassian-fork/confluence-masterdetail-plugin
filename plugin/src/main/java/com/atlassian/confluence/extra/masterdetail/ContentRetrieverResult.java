package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;

import java.util.List;

/**
 * @since 5.2.5
 */
public class ContentRetrieverResult {
    private final List<ContentEntityObject> rows;
    private final boolean limited;

    public ContentRetrieverResult(List<ContentEntityObject> rows, Boolean limited) {
        this.rows = rows;
        this.limited = limited;
    }

    public List<ContentEntityObject> getRows() {
        return rows;
    }

    public boolean isLimited() {
        return limited;
    }
}
