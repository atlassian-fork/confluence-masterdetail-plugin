package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static com.atlassian.confluence.extra.masterdetail.cqlmigrator.CompositeQueryExpression.BooleanOperator.AND;
import static com.atlassian.confluence.extra.masterdetail.cqlmigrator.ParameterSanitiser.getParameter;

/**
 * Converts legacy detailssummary instances to use CQL.
 */
public class DetailsSummaryMacroCqlSchemaMigrator implements MacroMigration {
    private static final Logger log = LoggerFactory.getLogger(DetailsSummaryMacroCqlSchemaMigrator.class);

    @Override
    public MacroDefinition migrate(MacroDefinition macro, ConversionContext context) {
        // Shouldn't need to check existing cql if schema version is being checked - just being a bit defensive while
        // Confluence core catches up.
        if (Strings.isNullOrEmpty(macro.getParameter("cql"))) {
            String cql = getCql(macro);

            macro.setParameter("cql", cql);
            macro.setTypedParameter("cql", cql);
            log.debug("CQL parameter set to '{}'", cql);
        }

        macro.setSchemaVersion(2);

        return macro;
    }

    private String getCql(MacroDefinition macro) {
        CompositeQueryExpression.Builder builder = CompositeQueryExpression.builder(AND);

        builder.add(getLabelExpression(macro));
        builder.add(getSpaceExpression(macro));

        return builder.build().toQueryString();
    }

    private QueryExpression getSpaceExpression(MacroDefinition macro) {
        String spaces = getParameter("spaces", macro);
        List<String> spaceKeys = ParameterSanitiser.getSpaceKeysFromDelimitedString(spaces);
        if (spaceKeys.isEmpty()) {
            spaceKeys.add("currentSpace()");  // TODO - expose CQL function parsing from the CQL library
        } else if (spaceKeys.contains("@all")) {
            return EmptyQueryExpression.EMPTY; // all spaces!
        }

        return SimpleQueryExpression.of("space", spaceKeys);
    }

    private QueryExpression getLabelExpression(MacroDefinition macro) {
        String label = getParameter("label", macro);
        return SimpleQueryExpression.of("label", label);
    }
}
