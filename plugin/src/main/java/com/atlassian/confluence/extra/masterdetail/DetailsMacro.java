package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Streamable;
import com.atlassian.confluence.content.render.xhtml.Streamables;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.StreamableMacro;
import com.atlassian.confluence.macro.StreamableMacroAdapter;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import java.util.Map;

import static com.atlassian.confluence.content.render.xhtml.Streamables.empty;
import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.PARAM_ID;
import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.PARAM_ID_MAX_LENGTH;
import static com.atlassian.confluence.macro.Macro.BodyType.RICH_TEXT;
import static com.atlassian.confluence.macro.Macro.OutputType.BLOCK;
import static com.atlassian.renderer.v2.RenderUtils.blockError;
import static org.apache.commons.lang3.BooleanUtils.toBoolean;

public class DetailsMacro extends BaseMacro implements StreamableMacro {
    public static final String MASTERDETAIL_RESOURCES = "confluence.extra.masterdetail:master-details-resources";

    private final PageBuilderService pageBuilderService;
    private final I18nResolver i18nResolver;

    public DetailsMacro(
            @ComponentImport PageBuilderService pageBuilderService,
            @ComponentImport final I18nResolver i18nResolver) {
        this.pageBuilderService = pageBuilderService;
        this.i18nResolver = i18nResolver;
    }

    @Override
    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException macroError) {
            throw new MacroException(macroError);
        }
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.ALL;
    }

    @Override
    public boolean hasBody() {
        return true;
    }

    @Override
    public TokenType getTokenType(Map parameters, String body, RenderContext context) {
        return TokenType.BLOCK;
    }

    @Override
    public Streamable executeToStream(final Map<String, String> parameters, final Streamable body, final ConversionContext context)
            throws MacroExecutionException {
        final String id = parameters.get(PARAM_ID);
        if (id != null && id.length() > PARAM_ID_MAX_LENGTH)
            return Streamables.from(blockError(i18nResolver.getText("details.error.id.length"), ""));

        if (!toBoolean(parameters.get("hidden"))) {
            pageBuilderService.assembler().resources().requireWebResource(MASTERDETAIL_RESOURCES);

            return writer -> {
                writer.write("<div class='plugin-tabmeta-details'>");
                body.writeTo(writer);
                writer.write("</div>");
            };
        }
        return empty();
    }

    @Override
    public String execute(final Map<String, String> parameters, final String body, final ConversionContext context)
            throws MacroExecutionException {
        return StreamableMacroAdapter.executeFromStream(this, parameters, body, context);
    }

    @Override
    public BodyType getBodyType() {
        return RICH_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return BLOCK;
    }
}
