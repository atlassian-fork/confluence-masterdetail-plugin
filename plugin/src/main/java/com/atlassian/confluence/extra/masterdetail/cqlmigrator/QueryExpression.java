package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

/**
 * Represents a simple or composite query Expression.
 */
public interface QueryExpression {
    /**
     * Serialises this expression to a query string.
     */
    String toQueryString();
}
