package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.CachingDetailsManager;
import com.atlassian.confluence.extra.masterdetail.ExtractedDetails;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroInstance;
import com.atlassian.confluence.plugins.pageproperties.api.model.PagePropertiesMacroReport;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.plugins.pageproperties.api.service.PagePropertiesService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent.Type.SERVICE_EXECUTION;
import static com.google.common.collect.Lists.newArrayListWithCapacity;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.collections.CollectionUtils.containsAny;
import static org.apache.commons.lang3.StringUtils.isBlank;

@Component
@ExportAsService(PagePropertiesService.class)
public class DefaultPagePropertiesService implements InternalPagePropertiesService {
    private final CachingDetailsManager cachingDetailsManager;
    private final PagePropertiesExtractor pagePropertiesExtractor;

    @Autowired
    public DefaultPagePropertiesService(final CachingDetailsManager cachingDetailsManager,
                                        final PagePropertiesExtractor pagePropertiesExtractor) {
        this.cachingDetailsManager = cachingDetailsManager;
        this.pagePropertiesExtractor = pagePropertiesExtractor;
    }

    @Override
    public PagePropertiesMacroReport getReportFromContent(final ContentEntityObject contentEntity) {
        final DetailsSummaryMacroMetricsEvent.Builder metrics = DetailsSummaryMacroMetricsEvent.builder(
                SERVICE_EXECUTION);
        final ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> detailsMap =
                extractPropertiesFromContent(contentEntity, metrics);

        final Map<String, List<PagePropertiesMacroInstance>> macroInstancesMap = new HashMap<>();
        detailsMap.forEach((detailsId, pageProperties) -> macroInstancesMap.put(detailsId,
                ImmutableList.copyOf(pageProperties.stream().map(PagePropertiesMacroInstance::new).collect(toList()))));

        return new PagePropertiesMacroReport(macroInstancesMap);
    }

    public List<ExtractedDetails> getDetailsFromContent(final Collection<ContentEntityObject> contents,
                                                        String detailsId,
                                                        final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        final List<ExtractedDetails> detailsList = newArrayListWithCapacity(contents.size());

        contents.forEach(entity -> detailsList.addAll(
                aggregatePageProperties(detailsId, extractPropertiesFromContent(entity, metrics))
                        .stream()
                        .map(pageProperties -> new ExtractedDetails(entity, pageProperties))
                        .collect(toList())));

        return detailsList;
    }

    private List<Map<String, PageProperty>> aggregatePageProperties(String detailsId,
                                                                    Map<String, ? extends List<ImmutableMap<String,
                                                                            PageProperty>>> detailsByIdMap) {
        final List<Map<String, PageProperty>> result = Lists.newArrayList();
        if (isBlank(detailsId)) {
            // if ID is not specified, we get all the properties from the page
            detailsByIdMap.values().forEach(value -> combineResults(value, result));
        } else {
            combineResults(detailsByIdMap.get(detailsId), result);
        }

        return result;
    }

    private void combineResults(List<ImmutableMap<String, PageProperty>> incoming,
                                List<Map<String, PageProperty>> detailMapList) {
        if (incoming == null)
            return;

        incoming.forEach(detailMap -> {
            if (detailMapList.isEmpty() || hasDupeKeys(detailMapList, detailMap)) {
                //we add new row in the scenarios either there is no row yet or the current details map has duplicate
                // keys with another row in the list.
                detailMapList.add(Maps.newHashMap(detailMap));  // make mutable so that we can do the putAll below.
            } else {
                detailMapList.get(detailMapList.size() - 1).putAll(detailMap);
            }
        });
    }

    private boolean hasDupeKeys(List<Map<String, PageProperty>> detailMapList, Map<String, PageProperty> details) {
        final Set<String> incomingKeys = details.keySet();
        return detailMapList.stream().anyMatch(existingMap -> containsAny(existingMap.keySet(), incomingKeys));
    }

    ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> extractPropertiesFromContent(final ContentEntityObject contentEntity,
                                                                                                         final DetailsSummaryMacroMetricsEvent.Builder metrics) {
        return cachingDetailsManager.get(contentEntity.getId(), () ->
                pagePropertiesExtractor.extractProperties(contentEntity, metrics)
        );
    }
}
