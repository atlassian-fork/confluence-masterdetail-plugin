package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.ExtractedDetails;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.plugins.pageproperties.api.service.PagePropertiesService;

import java.util.Collection;
import java.util.List;

/**
 * This internal service responsible for extracting all page properties for given contents {@link ContentEntityObject}. Tables
 * in Page properties macros are converted into a map of details id to properties keyed by heading text to
 * {@link PageProperty}.
 */
public interface InternalPagePropertiesService extends PagePropertiesService {
    /**
     * Get details (extracted from tables in Page properties macros) for given contents, if a detailsId is provided only
     * gets details of that special Page properties macro in a content.
     *
     * @param contents  the {@link Collection} of {@link ContentEntityObject} need to extract the detail {@link PageProperty}
     * @param detailsId if provided only get Details of the special Page properties macro.
     * @param metrics   runtime metrics {@link DetailsSummaryMacroMetricsEvent}
     * @return List of {@link ExtractedDetails} if a content has Page properties macro then table inside
     * Page properties macro will be extracted into a map of detail id and {@link PageProperty} filtered by detailsId if
     * provided and add into the {@link ExtractedDetails} List.
     */
    List<ExtractedDetails> getDetailsFromContent(final Collection<ContentEntityObject> contents, String detailsId,
                                                 final DetailsSummaryMacroMetricsEvent.Builder metrics);
}