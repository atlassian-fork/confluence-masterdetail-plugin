package com.atlassian.confluence.extra.masterdetail.rest;

import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.ContentRetriever;
import com.atlassian.confluence.extra.masterdetail.DetailsSummaryBuilder;
import com.atlassian.confluence.extra.masterdetail.DetailsSummaryParameters;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.extra.masterdetail.entities.PaginatedDetailLines;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.lang3.StringUtils;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.DEFAULT_PAGE_SIZE;
import static com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent.Type.REST_RESOURCE;
import static com.atlassian.confluence.extra.masterdetail.rest.ResourceErrorType.PARAMETER_INVALID;
import static com.atlassian.confluence.extra.masterdetail.rest.ResourceErrorType.PARAMETER_MISSING;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@AnonymousAllowed
@Path("detailssummary")
public class DetailsSummaryResource {
    private final DetailsSummaryBuilder detailsSummaryBuilder;
    private final ContentRetriever contentRetriever;
    private final EventPublisher eventPublisher;

    public DetailsSummaryResource(
            @ComponentImport final EventPublisher eventPublisher,
            final ContentRetriever contentRetriever,
            final DetailsSummaryBuilder detailsSummaryBuilder) {
        this.eventPublisher = eventPublisher;
        this.contentRetriever = contentRetriever;
        this.detailsSummaryBuilder = detailsSummaryBuilder;
    }

    @GET
    @Path("lines")
    @Produces({APPLICATION_JSON})
    public DetailsSummaryLines getDetailLines(
            @QueryParam("cql") final String cql,
            @QueryParam("detailsId") final String detailsId,
            @QueryParam("sortBy") final String sortBy,
            @QueryParam("reverseSort") @DefaultValue("false") final boolean reverseSort,
            @QueryParam("spaceKey") final String contextSpaceKey,
            @QueryParam("contentId") final ContentId contentId,
            @QueryParam("pageSize") @DefaultValue("" + DEFAULT_PAGE_SIZE) final int pageSize,
            @QueryParam("pageIndex") @DefaultValue("0") final int currentPage,
            @QueryParam("countComments") @DefaultValue("false") final boolean countComments,
            @QueryParam("countLikes") @DefaultValue("false") final boolean countLikes,
            @QueryParam("headings") final String headings) {
        // Required for calculating current space if the CQL doesn't explicitly specify one.
        if (StringUtils.isEmpty(contextSpaceKey))
            throw new ResourceException("'spaceKey' parameter is required", BAD_REQUEST, PARAMETER_MISSING, "spaceKey");

        if (pageSize <= 0)
            throw new ResourceException("Requested page size is not valid", BAD_REQUEST, PARAMETER_INVALID, "pageSize");

        if (currentPage < 0)
            throw new ResourceException("Requested page index is not valid", BAD_REQUEST, PARAMETER_INVALID, "pageIndex");

        try {
            final DetailsSummaryMacroMetricsEvent.Builder metrics = DetailsSummaryMacroMetricsEvent.builder(REST_RESOURCE);

            SearchContext searchContext = SearchContext.builder()
                    .spaceKey(contextSpaceKey)
                    .contentId(contentId)
                    .build();
            final List<ContentEntityObject> content = contentRetriever.getContentWithMetaData(cql, reverseSort, searchContext, metrics).getRows();
            final DetailsSummaryParameters summaryParams = new DetailsSummaryParameters()
                    .setPageSize(pageSize)
                    .setCurrentPage(currentPage)
                    .setCountComments(countComments)
                    .setCountLikes(countLikes)
                    .setHeadingsString(headings)
                    .setSortBy(sortBy)
                    .setReverseSort(reverseSort)
                    .setContent(content)
                    .setId(detailsId);

            final PaginatedDetailLines paginatedDetailLines = detailsSummaryBuilder.getPaginatedDetailLines(
                    summaryParams, true, metrics, ConversionContextOutputType.DISPLAY.value());

            eventPublisher.publish(metrics.build());

            return new DetailsSummaryLines(
                    summaryParams.getCurrentPage(),
                    summaryParams.getTotalPages(),
                    paginatedDetailLines.getRenderedHeadings(),
                    paginatedDetailLines.getDetailLines(),
                    paginatedDetailLines.isAsyncRenderSafe()
            );
        } catch (MacroExecutionException e) {
            throw new ResourceException(e, Response.Status.INTERNAL_SERVER_ERROR, ResourceErrorType.RENDERING_MACRO);
        }
    }
}
