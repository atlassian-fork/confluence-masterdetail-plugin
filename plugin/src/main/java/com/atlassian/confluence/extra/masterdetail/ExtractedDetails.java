package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;

import java.util.Map;

/**
 * Simple object to represent the extracted details for an object
 */
public class ExtractedDetails {
    final private ContentEntityObject content;
    private final Map<String, PageProperty> details;

    public ExtractedDetails(ContentEntityObject content, Map<String, PageProperty> details) {
        this.content = content;
        this.details = details;
    }

    public String getTitle() {
        if (content != null)
            return content.getTitle();

        return null;
    }

    public ContentEntityObject getContent() {
        return content;
    }

    /**
     * A map of details keyed by heading text to ExtractedDetail. Note that headings will be just plain text and not
     * include html markup. You can get the heading storage format from the ExtractedDetail.
     */
    public Map<String, PageProperty> getDetails() {
        return details;
    }

    public String getDetailStorageFormat(String heading) {
        if (details == null)
            return "";

        PageProperty pageProperty = details.get(heading);
        return pageProperty == null ? "" : pageProperty.getDetailStorageFormat();

    }
}
