package com.atlassian.confluence.extra.masterdetail;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;

import java.util.Map;

import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.DEFAULT_PAGE_SIZE;
import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.MAX_PAGESIZE;
import static com.atlassian.confluence.extra.masterdetail.DetailsSummaryMacro.PARAM_PAGE_SIZE;

public enum DetailsSummaryRenderingStrategy {
    CLIENT_SIDE,
    SERVER_SIDE;

    private static int calculatePageSizeWithMaximum(Map<String, String> macroParameters, int internalMax) {
        return Math.min(internalMax, getConfiguredPageSize(macroParameters));
    }

    private static int getConfiguredPageSize(Map<String, String> macroParameters) {
        try {
            return Integer.parseInt(macroParameters.get(PARAM_PAGE_SIZE));
        } catch (NumberFormatException e) {
            return DEFAULT_PAGE_SIZE;
        }
    }

    /**
     * Decides if we should be rendering the content server-side or client-side.
     * NASTY HACK: For normal page rendering, we default to client-side rendering, but the client may detect
     * non-client-side-friendly content and force a page fresh with a request parameter which forces server-side
     * rendering.
     */
    static DetailsSummaryRenderingStrategy strategyFor(ConversionContext conversionContext) {
        final boolean isDisplayOutput = ConversionContextOutputType.DISPLAY.value().equals(conversionContext.getOutputType());
        final boolean asyncRenderSafe = conversionContext.isAsyncRenderSafe();
        return isDisplayOutput && asyncRenderSafe ? CLIENT_SIDE : SERVER_SIDE;
    }

    public int calculatePageSize(Map<String, String> macroParameters) {
        return calculatePageSizeWithMaximum(macroParameters, MAX_PAGESIZE);
    }
}
