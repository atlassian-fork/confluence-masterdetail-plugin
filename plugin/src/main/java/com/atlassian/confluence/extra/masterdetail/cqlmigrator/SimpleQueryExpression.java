package com.atlassian.confluence.extra.masterdetail.cqlmigrator;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.confluence.extra.masterdetail.cqlmigrator.SimpleQueryExpression.InclusionOperator.INCLUDES;
import static com.google.common.base.Joiner.on;

/**
 * Represents an atomic CQL expression with an operator '=', '!=' 'IN' or 'NOT IN' string, but no AND/OR operators.
 */
public class SimpleQueryExpression implements QueryExpression {
    private final String key;
    private final InclusionOperator inclusionOperator;
    private final List<String> values;

    private SimpleQueryExpression(String key, InclusionOperator inclusionOperator, List<String> values) {
        this.key = key;
        this.inclusionOperator = inclusionOperator;
        this.values = ImmutableList.copyOf(values);
    }

    private SimpleQueryExpression(String key, InclusionOperator inclusionOperator, String... values) {
        this(key, inclusionOperator, Arrays.asList(values));
    }

    public String getKey() {
        return key;
    }

    public InclusionOperator getInclusionOperator() {
        return inclusionOperator;
    }

    public List<String> getValues() {
        return values;
    }

    @Override
    public String toQueryString() {
        String operator;
        boolean multiValue = values.size() > 1;
        if (inclusionOperator == INCLUDES) {
            operator = multiValue ? "in" : "=";
        } else {
            operator = multiValue ? "not in" : "!=";
        }

        String prefix = key + " " + operator + " ";

        if (!multiValue)
            return prefix + wrapWithQuotes.apply(values.get(0));

        List<String> wrapped = Lists.transform(values, wrapWithQuotes);
        return prefix + "(" + on(',').join(wrapped) + ")";
    }

    public static SimpleQueryExpression of(String key, String... values) {
        return new SimpleQueryExpression(key, INCLUDES, values);
    }

    public static SimpleQueryExpression of(String key, List<String> values) {
        return new SimpleQueryExpression(key, INCLUDES, values);
    }

    public static SimpleQueryExpression of(String key, InclusionOperator inclusionOperator, String... values) {
        return new SimpleQueryExpression(key, inclusionOperator, values);
    }

    public static SimpleQueryExpression of(String key, InclusionOperator inclusionOperator, List<String> values) {
        return new SimpleQueryExpression(key, inclusionOperator, values);
    }

    /**
     * InclusionOperator determines whether a value should be included or excluded from results.
     */
    public static enum InclusionOperator {
        INCLUDES,
        EXCLUDES
    }

    static Function<String, String> wrapWithQuotes = input -> {
        if (input.equals("currentSpace()"))
            return input;  // this is the only function expected during migration - if we get any more cases
        // we might need to consider a "CQLFunction" expression value type.

        return "\"" + input.replaceAll("\"", "\\\\\"") + "\"";
    };
}
