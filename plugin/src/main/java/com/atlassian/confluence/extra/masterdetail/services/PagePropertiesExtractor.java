package com.atlassian.confluence.extra.masterdetail.services;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.masterdetail.analytics.DetailsSummaryMacroMetricsEvent;
import com.atlassian.confluence.plugins.pageproperties.api.model.PageProperty;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.confluence.xhtml.api.MacroDefinitionMarshallingStrategy.MARSHALL_MACRO;
import static org.apache.commons.lang3.StringUtils.defaultString;

@Component
public class PagePropertiesExtractor {
    private static final Logger log = LoggerFactory.getLogger(DefaultPagePropertiesService.class);

    private final XhtmlContent xhtmlContent;

    @Autowired
    public PagePropertiesExtractor(@ComponentImport final XhtmlContent xhtmlContent) {
        this.xhtmlContent = xhtmlContent;
    }

    public ImmutableMap<String, ImmutableList<ImmutableMap<String, PageProperty>>> extractProperties(ContentEntityObject entity,
                                                                                                     DetailsSummaryMacroMetricsEvent.Builder metrics) {
        // We need to marshall non-detail-macros encountered in the body of the detail macro, so that these macros
        // can be rendered in the table of detail lines.
        final DefaultConversionContext subContext = new DefaultConversionContext(entity.toPageContext());
        final DetailsMacroBodyHandler macroBodyHandler = new DetailsMacroBodyHandler(metrics);

        try {
            metrics.entityBodyFetchStart();
            final String entityBody = entity.getBodyAsString();
            metrics.entityBodyFetchFinish(defaultString(entityBody).length());

            xhtmlContent.handleMacroDefinitions(entityBody, subContext, macroBodyHandler, MARSHALL_MACRO);
        } catch (XhtmlException e) {
            log.error("Cannot extract page properties from content with id: " + entity.getIdAsString(), e);
        }

        return macroBodyHandler.getDetails();
    }
}
