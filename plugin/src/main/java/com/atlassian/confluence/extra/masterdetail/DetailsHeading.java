package com.atlassian.confluence.extra.masterdetail;

public class DetailsHeading {
    final private String heading;
    final private String renderedHeading;

    public DetailsHeading(String heading, String renderedHeading) {
        this.heading = heading;
        this.renderedHeading = renderedHeading;
    }

    public String getHeading() {
        return heading;
    }

    public String getRenderedHeading() {
        return renderedHeading;
    }
}
