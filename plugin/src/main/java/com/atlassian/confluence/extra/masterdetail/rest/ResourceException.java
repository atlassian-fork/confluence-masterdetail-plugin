package com.atlassian.confluence.extra.masterdetail.rest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

// FIXME: Copied from Create Content plugin!
public class ResourceException extends WebApplicationException {
    public ResourceException(@Nonnull final Throwable cause, @Nonnull final Response.Status status, @Nonnull final ResourceErrorType errorType) {
        this(cause, status, errorType, null);
    }

    public ResourceException(@Nonnull final Throwable cause, @Nonnull final Response.Status status, @Nonnull final ResourceErrorType errorType, @Nullable final Object errorData) {
        // We want the message for the response, and the original cause for the stack-trace.
        super(cause, makeResponse(cause.getMessage(), status, errorType, errorData));
    }

    public ResourceException(@Nonnull final String cause, @Nonnull final Response.Status status, @Nonnull final ResourceErrorType errorType) {
        this(cause, status, errorType, null);
    }

    public ResourceException(@Nonnull final String cause, @Nonnull final Response.Status status, @Nonnull final ResourceErrorType errorType, @Nullable final Object errorData) {
        super(makeResponse(cause, status, errorType, errorData));
    }

    static Response makeResponse(@Nonnull final String cause, @Nonnull final Response.Status status, @Nonnull final ResourceErrorType errorType, @Nullable final Object errorData) {
        return Response.status(status).entity(new ResourceErrorBean(status.getStatusCode(), errorType.getValue(), errorData, cause)).build();
    }
}
