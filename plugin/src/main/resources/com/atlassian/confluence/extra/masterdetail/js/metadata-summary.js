AJS.$(function ($) {

    var removeBraces = function (str) {
        if (str) {
            return ("" + str).replace(/[\[\]]/g, "");
        }

        return "";
    };

    // Load rendered table data via a REST call if it isn't pre-rendered (see DetailsSummaryRenderingStrategy.java).
    $('.metadata-summary-macro').each(function () {
        var $table = $(this);

        var $outerContainer = $table.parent(),
            showCommentsCount = $table.data('count-comments'),
            showLikesCount = $table.data('count-likes'),
            pageSize = $table.data('page-size'),
            totalPages = $table.data('total-pages'),
            headings = $table.data('headings'),
            firstColumnHeading = $table.data('first-column-heading'),
            sortBy = removeBraces($table.data('sort-by')),
            reverseSort = removeBraces($table.data('reverse-sort')),
            blueprintPresent = $table.data('blueprint-present'),
            spaces = removeBraces($table.data('spaces'));

        function addLine(line, $container) {
            var renderedLine = Confluence.Templates.Macro.MasterDetail.detailsSummaryLine({
                'line': line,
                'showCommentsCount': showCommentsCount,
                'showLikesCount': showLikesCount
            });

            $container.append(renderedLine);
            $table.trigger("update"); // fix table header sorting
        }

        function addLineInBatch(line) {
            return Confluence.Templates.Macro.MasterDetail.detailsSummaryLine({
                'line': line,
                'showCommentsCount': showCommentsCount,
                'showLikesCount': showLikesCount
            });
        }

        function renderTableHeader(headers, $container) {
            var renderedLine = Confluence.Templates.Macro.MasterDetail.detailsSummaryHeader({
                'firstColumnHeading': firstColumnHeading,
                'renderedHeadings': headers
            });

            $container.append(renderedLine);
        }

        function renderNoResults(headers, $container) {
            var renderedLine = Confluence.Templates.Macro.MasterDetail.detailsSummaryNoResults({
                'headingCount': headers.length
            });

            $container.append(renderedLine);
        }

        var options = {
            'scope': $outerContainer,
            'pageSize': pageSize,
            'totalPages': totalPages,
            'path': "/rest/masterdetail/1.0/detailssummary/lines",
            'data': {
                'cql': $table.data('cql'),
                'spaceKey': AJS.Meta.get('space-key'),
                'contentId': AJS.Meta.get('page-id'),
                'detailsId': $table.data('details-id'),
                'headings': headings,
                'countComments': showCommentsCount,
                'countLikes': showLikesCount,
                'sortBy': sortBy,
                'reverseSort': reverseSort
            },
            'success': addLine
        };

        var $tableCaption = $table.find('caption');

        // Function called when the initial page of results is loaded.
        // Subsequent pages are handled by Confluence.UI.Components.Pagination and call addLine directly
        function success(data) {
            $tableCaption.hide();

            if (data.detailLines.length === 0 && blueprintPresent) {
                $($outerContainer).find('div.blueprint-blank-experience').show();

            } else {
                $outerContainer.hide();

                renderTableHeader(data.renderedHeadings, $table.find('thead'));

                var $tbody = $table.find('tbody');
                if (data.detailLines.length === 0) {
                    renderNoResults(data.renderedHeadings, $tbody);
                } else {
                    AJS.tablessortable.setTableSortable($table);

                    var renderedLines = "";
                    _.each(data.detailLines, function (line) {
                        renderedLines += addLineInBatch(line);
                    });
                    $tbody.append(renderedLines);
                    $table.trigger("update"); // fix table header sorting
                }
                if (data.totalPages > 1) {
                    options.totalPages = data.totalPages;
                    // This will decorate our table with pagination controls
                    Confluence.UI.Components.Pagination.build(options);
                }

                // A plain "show" would cause a content jump as the table suddenly appeared... be gentle
                $outerContainer.slideDown();
            }
        }

        function failure(jqXHR, textStatus, errorThrown) {
            $tableCaption.text(AJS.I18n.getText("detailssummary.error.ajax") + ": " + errorThrown);
        }

        //now perform the fetch for the first page of results, and populate the table with them
        var requestData = $.extend({}, {
            'pageSize': pageSize,
            'pageIndex': 0
        }, options.data);

        var requestUrl = AJS.contextPath() + options.path;

        // Table header row directly inside this table indicates server-side rendered content
        var isPreRendered = $table.children('thead').children('tr').length !== 0;
        if (isPreRendered) {
            // for pre-rendered content, add the paging controls immediately
            if (totalPages > 1) {
                Confluence.UI.Components.Pagination.build(options);
            }
        } else {
            $.getJSON(requestUrl, requestData).done(success).fail(failure);
        }
    });
});