AJS.$(function ($) {
    /*
     CONFDEV-11638 We want to know if people are using the 'Collector' pages as a
     navigational tool, so track clicks to pages. Only Summary macros created by
     us - with the hidden param 'analytics-key' - will be tracked, to try and reduce
     noise from collector pages the user creates themselves.
     */
    $('.metadata-summary-macro[data-analytics-key]').on('click', '.title a', function (event) {
        // (delegateTarget is the table, target is the anchor)
        var analyticsKey = $(event.delegateTarget).data('analytics-key');
        var eventName = 'metadata-summary-macro.content-click.' + analyticsKey;
        var contentId = $(this).parent().data('content-id');
        AJS.trigger('analytics', {name: eventName, data: {contentId: contentId}});
    });
});